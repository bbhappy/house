import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class MYSQLConnectionTest {
    private static final String DRIVER = "com.mysql.cj.jdbc.Driver"; //Connection을 구현한 클래스의 이름
    private static final String URL = "JDBC:MYSQL://127.0.0.1:3306/"; //mysql 서버 주소
    private static final String USER = "root"; // 계정
    private static final String PW = ""; // 비밀번호

    @Test //jUnit이 테스트함
    public void testConnection() throws Exception{
        Class.forName(DRIVER); //DRIVER라는 이름을 가진 클래스를 찾음

        try(Connection con = DriverManager.getConnection(URL,USER,PW)){ //DB 계정과 연결하여 연결된 객체를 Connection 클래스의 인스턴스인 con에 담음
            int num = insert(con, "22", "22", "22", "22", "2021-01-23" );
            System.out.println(num+"개 행 삽입 완료");
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

    //INSERT 문을 날리는 메서드 (id는 db에서 자동으로 생성하기에 넣을 필요 없음, 반환값은 넣은 행의 개수)
    private int insert(Connection con , String user_id , String user_pw , String user_nm , String user_tel , String req_dt ) {
        final String SQL= "INSERT INTO `chains`.`user` (`user_id`, `user_pw`, `user_nm`, `user_tel`, `req_dt`) " +
                "VALUES (?,?,?,?,?);"; //sql 쿼리

        //PreparedStatement에서 해당 SQL을 미리 컴파일함
        try(PreparedStatement pstml = (PreparedStatement) con.prepareStatement(SQL)) {
            pstml.setString(1, user_id); //1번째 물음표에 user_id 삽입
            pstml.setString(2, user_pw); //2번째 물음표에 user_pw 삽입
            pstml.setString(3, user_nm);
            pstml.setString(4, user_tel);
            pstml.setString(5, req_dt);
            return pstml.executeUpdate(); //쿼리실행 반환 값 삽입한 행의 개수
        }catch(Exception e){ //예외처리
            e.printStackTrace();
            System.out.println("테이블에 행 삽입 실패");
            return 0;
        }
    }

    // SELECT 문을 날리는 메서드
    private static final String SQL2 = "select * from `chains`.`user` where user_id = ?;"; // sql 쿼리

    public String selectName(Connection con, Integer id) throws Exception {
        String result = null;
        try (PreparedStatement pstmt = con.prepareStatement(SQL2)) {
            pstmt.setString(1,id.toString());
            ResultSet rs = pstmt.executeQuery(); // 쿼리 실행
            if (rs.next()) // 다음행이 있는지 확인
                result = rs.getString("name"); // name column을 가져옴
        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    // DELETE 문을 날리는 메서드
    private static final String SQL3 = "delete from `chains`.`user` where user_id = ?;"; // sql 쿼리

    public int delete(Connection con, Integer id) throws Exception {
        int result = 0;
        try (PreparedStatement pstmt = con.prepareStatement(SQL3)) {
            pstmt.setString(1,id.toString());
            result = pstmt.executeUpdate(); // 쿼리 실행
        } catch (Exception e) {
            throw e;
        }
        return result;
    }

}
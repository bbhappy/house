package com.house.handler.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class BoardDTO {

    private int idx;
    private String writer;
    private String writer_id;
    private String title;
    private String content;
    private LocalDateTime req_dt;
    private int view;
}

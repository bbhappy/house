package com.house.handler.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class MenuDTO {
    private Integer mm_seq;
    private String mm_parent;
    private String mm_code;
    private String mm_level;
    private String mm_module;
    private String mm_name;
    private String mm_navi;
    private String mm_path;
    private String mm_icon;
    private Integer mm_sort;
    private String mm_is_use;
    private LocalDateTime mm_reg_date;
    private MenuAuthDTO menuAuth;
}


package com.house.handler.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class LogDTO {
    private String user_req;
    private String user_ip;
    private String user_id;
    private String user_nm;
    private LocalDateTime req_dt;
}

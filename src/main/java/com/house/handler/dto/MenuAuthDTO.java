package com.house.handler.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class MenuAuthDTO {
 
    private Integer mmu_seq;
    private String emp_id;
    private String mm_parent;
    private String mm_code;
    private String mu_view;
    private String mu_new;
    private String mu_mod;
    private String mu_del;
    private String mu_show;
    private LocalDateTime reg_date;
}

package com.house.handler.dto;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class UserDTO {
    private String user_no;
    private String user_id;
    private String user_pw;
    private String user_nm;
    private String user_tel;
    private String user_salt;
    private String user_email;
    private String user_isshow;
    private int fail_count;
    private Date modify_pw;
    private Date req_dt;
    private String command;
}

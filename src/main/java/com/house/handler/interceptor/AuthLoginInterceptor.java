package com.house.handler.interceptor;

import com.house.handler.dto.LogDTO;
import com.house.handler.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;

public class AuthLoginInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private LogService logService;

    // preHandle() : 컨트롤러보다 먼저 수행되는 메서드
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {

        HttpSession session = request.getSession();
        Object obj = session.getAttribute("user_id");

        if ( obj == null ){
            // 로그인이 안되어 있는 상태는 redirect
            response.sendRedirect(request.getContextPath() + "/");
            return false; // 더이상 컨트롤러 요청으로 가지 않도록 false로 반환
        }else{

            // 로그 남기기
            LogDTO log = new LogDTO();
            log.setUser_req(request.getRequestURI());
            log.setUser_ip(request.getServerName());
            log.setUser_id((String)session.getAttribute("user_id"));
            log.setUser_nm((String)session.getAttribute("user_nm"));
            log.setReq_dt(LocalDateTime.now());
            logService.logUpdate(log);
        }


        return true; // preHandle의 return은 컨트롤러 요청 uri로 이동 하는것을 허가하는 의미 ( true로하면 컨트롤러 uri로 이동 )
    }

    //컨트롤러가 수행되고 화면이 보여지기 직전에 수행되는 메서드
//    @Override
//    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
//                           ModelAndView modelAndView) throws Exception {
//
//        super.postHandle(request, response, handler, modelAndView);
//    }


}

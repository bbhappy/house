package com.house.handler.repository;

import com.house.handler.dto.BoardDTO;
import com.house.handler.dto.UserDTO;
import com.house.handler.paging.Criteria;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class BoardDAO extends SqlSessionDaoSupport {

    @Autowired
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
        super.setSqlSessionFactory(sqlSessionFactory);
    }

    public int insertBoard(BoardDTO boardDTO) {
        getSqlSession().insert("insertBoard",boardDTO);
        return boardDTO.getIdx();
    }

    public Map<String, Object> listBoard(Criteria criteria, BoardDTO boardDTO) {

        int pageNum = criteria.getPageNum();
        int amount = criteria.getAmount();
        if(pageNum < 0) pageNum = 1;
        int startNum = (pageNum-1) * amount;

        SqlSession session = getSqlSessionFactory().openSession();
        Map<String,Object> map2 = new LinkedHashMap<>();
        try{
            HashMap<String,Object> map = new HashMap();
            map.put("startNum",startNum);
            map.put("endNum",amount);
            map.put("boardDTO",boardDTO);

            List<UserDTO> listBoard = session.selectList("listBoard" , map);
            int total_count = session.selectOne("queryRowCount2");
            map2.put("listBoard",listBoard);
            map2.put("total_count",total_count);
        }finally {
            session.close();
        }
        return map2;
    }

    public BoardDTO searchBoardOne(BoardDTO boardDTO) {
        return getSqlSession().selectOne("searchBoardOne",boardDTO);
    }

    public void boardViewUpdate(BoardDTO boardDTO) {
        getSqlSession().selectOne("boardViewUpdate",boardDTO);
    }

    public int deleteBoard(BoardDTO boardDTO) {
        return getSqlSession().delete("deleteBoard",boardDTO);
    }

    public int updateBoard(BoardDTO boardDTO) {
        return getSqlSession().update("updateBoard",boardDTO);
    }

}

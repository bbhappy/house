package com.house.handler.repository;

import com.house.handler.dto.MenuAuthDTO;
import com.house.handler.dto.MenuDTO;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class MenuDAO extends SqlSessionDaoSupport {

    @Autowired
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
        super.setSqlSessionFactory(sqlSessionFactory);
    }

    public List<MenuDTO> getAdminMenu() { return getSqlSession().selectList("getAdminMenu"); }
    public List<MenuDTO> menuAuthList(Map<String, Object> map) { return getSqlSession().selectList("menuAuthList" , map ); }
    public List<MenuDTO> allMenuList() { return getSqlSession().selectList("allMenuList"); }
    public List<MenuAuthDTO> userAuthList(String user_id) { return getSqlSession().selectList("userAuthList" , user_id); }
    public void updateMenuAuth(MenuAuthDTO menuAuthDTO) { getSqlSession().update("updateMenuAuth",menuAuthDTO); }
}

package com.house.handler.repository;

import com.house.handler.dto.BoardDTO;
import com.house.handler.dto.BoardReplyDTO;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BoardReplyDAO extends SqlSessionDaoSupport {

    @Autowired
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
        super.setSqlSessionFactory(sqlSessionFactory);
    }

    public List<BoardReplyDTO> boardReplyList(BoardDTO boardDTO) {
        return getSqlSession().selectList("boardReplyList",boardDTO);
    }

    public int parentReplyInsert(BoardReplyDTO boardReplyDTO) {

        SqlSession session = getSqlSessionFactory().openSession();
        int rowCount = session.insert("parentReplyInert", boardReplyDTO);
        int no =  Integer.parseInt(boardReplyDTO.getNo());
        int key = session.update("parentReplyCheck", no);
        session.close();
        return key;
    }

    public int childReplyInsert(BoardReplyDTO boardReplyDTO) {
        return getSqlSession().insert("childReplyInsert",boardReplyDTO);
    }

    public int parentReplyDelete(String no) {
        return getSqlSession().update("parentReplyDelete",no);
    }

    public int childReplyDelete(String no) {
        return getSqlSession().update("childReplyDelete",no);
    }
}

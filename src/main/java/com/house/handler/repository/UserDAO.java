package com.house.handler.repository;

import com.house.handler.dto.UserDTO;
import com.house.handler.paging.Criteria;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class UserDAO extends SqlSessionDaoSupport {

    @Autowired
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
        super.setSqlSessionFactory(sqlSessionFactory);
    }
    
    //사용자 등록
    public void insertUser(UserDTO userDTO) {
        getSqlSession().insert("insertUser" , userDTO);
    }
    
    //사용자 중복확인
    public boolean checkUserDuplicated(UserDTO userDTO) {
        return (Integer) getSqlSession().selectOne("checkUserDuplicated", userDTO) > 0 ? true : false;
    }

    //sault 값 가져오기
    public String getSalt(String user_id){
        return (String) getSqlSession().selectOne("getSalt" , user_id );
    }

    //로그인 시도 횟수 update
    public void updateFailCount(UserDTO userDTO){
        getSqlSession().update("updateFailCount", userDTO);
    }

    //사용자 1명 정보 찾기
    public UserDTO findByUserInfo( UserDTO userDTO ){
        return getSqlSession().selectOne("findByUserInfo", userDTO );
    }

    // 비밀번호 변경하기
    public int updateNewPw(UserDTO userDTO) {
        return getSqlSession().update("updateNewPw", userDTO );
    }

    //사용자 리스트
    public Map<String,Object> listUser(Criteria criteria , UserDTO userDTO){

        int pageNum = criteria.getPageNum();
        int amount = criteria.getAmount();
        if(pageNum < 0) pageNum = 1;
        int startNum = (pageNum-1) * amount;

        SqlSession session = getSqlSessionFactory().openSession();
        Map<String,Object> map2 = new LinkedHashMap<>();
        try{
            HashMap<String,Object> map = new HashMap();
            map.put("startNum",startNum);
            map.put("endNum",amount);
            map.put("userDTO",userDTO);

            List<UserDTO> listUser = session.selectList("listUser" , map);
            int total_count = session.selectOne("queryRowCount");
            map2.put("listUser",listUser);
            map2.put("total_count",total_count);
        }finally {
            session.close();
        }
        return map2;
    }

    // 로그인 실패횟수 초기화
    public int userLoginFailClear( String it_user_id ){ return getSqlSession().update("userLoginFailClear" , it_user_id); }
    
    // 사용자 탈퇴처리
    public int userLeave(String deleteList) {
        return getSqlSession().delete("userLeave", deleteList);
    }

    //사용자 정보 수정하기
    public int userInfoModify(UserDTO userDTO) { return getSqlSession().update("userInfoModify" , userDTO); }
}

package com.house.handler.service;

import com.house.handler.dto.LogDTO;
import com.house.handler.repository.LogDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("LogService")
public class LogServiceImpl implements LogService{

    @Autowired
    private LogDAO logDAO;

    @Override
    public void logUpdate(LogDTO logDTO) { logDAO.logUpdate(logDTO); }
}

package com.house.handler.service;

import com.house.handler.dto.UserDTO;
import com.house.handler.paging.Criteria;

import java.util.Map;

public interface UserService {

        void insert(UserDTO userDTO);
        boolean checkUserDuplicated(UserDTO userDTO);
        String getSalt(String user_id);
        void updateFailCount( UserDTO userDTO );
        UserDTO findByUserInfo( UserDTO userDTO );
        int updateNewPw( UserDTO userDTO );
        Map<String,Object> listUser(Criteria criteria , UserDTO userDTO);
        int userLoginFailClear( String it_user_id );
        int userLeave( String deleteList );
        int userInfoModify( UserDTO userDTO);
}

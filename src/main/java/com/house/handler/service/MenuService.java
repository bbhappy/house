package com.house.handler.service;

import com.house.handler.dto.MenuAuthDTO;
import com.house.handler.dto.MenuDTO;

import java.util.List;
import java.util.Map;

public interface MenuService {

    List<MenuDTO> getAdminMenu();                           // mm_level = 1 :: 대메뉴 리스트
    List<MenuDTO> menuAuthList(Map<String, Object> map);    // 로그인한 사용자가 사용할 수있는 메뉴 리스트( 대메뉴 , 소메뉴 포함)
    List<MenuDTO> allMenuList();                            // 모든 메뉴 리스트
    List<MenuAuthDTO> userAuthList(String user_id);         // 모든 사용자 권한 리스트
    void updateMenuAuth(MenuAuthDTO menuAuthDTO);           // 사용자 권한 업데이트
}

package com.house.handler.service;

import com.house.handler.dto.UserDTO;
import com.house.handler.paging.Criteria;
import com.house.handler.repository.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service("UserService")
public class UserServiceImpl implements UserService{

    @Autowired
    private UserDAO userDAO;

    public void insert(UserDTO userDTO) { userDAO.insertUser(userDTO); }                                                // 사용자 등록
    public boolean checkUserDuplicated(UserDTO userDTO) { return userDAO.checkUserDuplicated(userDTO); }                // 사용자 중복확인
    public String getSalt(String user_id){ return userDAO.getSalt(user_id); }                                           // salut 가져오기
    public void updateFailCount( UserDTO userDTO ){ userDAO.updateFailCount(userDTO); }                                 // 로그인 실패횟수 update
    public UserDTO findByUserInfo( UserDTO userDTO ){ return userDAO.findByUserInfo(userDTO); }                         // 사용자 1명 정보 찾기
    public int updateNewPw(UserDTO userDTO) { return userDAO.updateNewPw(userDTO); }                                    // 비밀번호 변경
    public Map<String,Object> listUser(Criteria criteria , UserDTO userDTO) { return userDAO.listUser(criteria , userDTO); }    // 사용자 리스트
    public int userLoginFailClear( String it_user_id ){ return userDAO.userLoginFailClear( it_user_id ); }              // 로그인 실패횟수 초기화 
    public int userLeave(String deleteList) { return userDAO.userLeave(deleteList); }                                   // 사용자 탈퇴처리
    public int userInfoModify(UserDTO userDTO) { return userDAO.userInfoModify(userDTO);  }                             // 사용자 정보 수정하기

}

package com.house.handler.service;

import com.house.handler.dto.LogDTO;

public interface LogService {

    void logUpdate(LogDTO logDTO);
}

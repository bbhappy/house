package com.house.handler.service;

import com.house.handler.dto.MenuAuthDTO;
import com.house.handler.dto.MenuDTO;
import com.house.handler.repository.MenuDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("MenuService")
public class MenuServiceImpl implements MenuService {

    @Autowired
    MenuDAO menuDAO;

    @Override
    public List<MenuDTO> getAdminMenu() {
        return menuDAO.getAdminMenu();
    }

    @Override
    public List<MenuDTO> menuAuthList(Map<String, Object> map) {
        return menuDAO.menuAuthList(map);
    }

    @Override
    public List<MenuDTO> allMenuList() {
        return menuDAO.allMenuList();
    }

    @Override
    public List<MenuAuthDTO> userAuthList(String user_id) { return menuDAO.userAuthList(user_id); }

    @Override
    public void updateMenuAuth(MenuAuthDTO menuAuthDTO) { menuDAO.updateMenuAuth(menuAuthDTO); }
}

package com.house.handler.service;

import com.house.handler.dto.BoardDTO;
import com.house.handler.dto.BoardReplyDTO;
import com.house.handler.paging.Criteria;

import java.util.List;
import java.util.Map;

public interface BoardService {

    int insertBoard(BoardDTO boardDTO);
    Map<String,Object> listBoard(Criteria criteria , BoardDTO boardDTO);
    BoardDTO searchBoardOne(BoardDTO boardDTO);
    void boardViewUpdate(BoardDTO boardDTO);
    int deleteBoard(BoardDTO boardDTO);
    int updateBoard(BoardDTO boardDTO);


    List<BoardReplyDTO> boardReplyList(BoardDTO boardDTO);
    int parentReplyInsert(BoardReplyDTO boardReplyDTO);
    int childReplyInsert(BoardReplyDTO boardReplyDTO);
    int parentReplyDelete(String no);
    int childReplyDelete(String no);
}

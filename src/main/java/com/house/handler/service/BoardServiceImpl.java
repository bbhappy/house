package com.house.handler.service;

import com.house.handler.dto.BoardDTO;
import com.house.handler.dto.BoardReplyDTO;
import com.house.handler.paging.Criteria;
import com.house.handler.repository.BoardDAO;
import com.house.handler.repository.BoardReplyDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("BoardService")
public class BoardServiceImpl implements BoardService{

    @Autowired
    private BoardDAO boardDAO;

    @Autowired
    private BoardReplyDAO boardReplyDAO;



    public int insertBoard(BoardDTO boardDTO) {   // 자유게시판 글 등록
        return boardDAO.insertBoard(boardDTO);
    }

    public Map<String, Object> listBoard(Criteria criteria, BoardDTO boardDTO) { // 자유게시판 리스트
        return boardDAO.listBoard(criteria , boardDTO);
    }

    public BoardDTO searchBoardOne(BoardDTO boardDTO) { // 선택한 게시글 상세보기
        return boardDAO.searchBoardOne(boardDTO);
    }

    @Override
    public void boardViewUpdate(BoardDTO boardDTO) { // 게시글 조회수 +1
        boardDAO.boardViewUpdate(boardDTO);
    }

    @Override
    public int deleteBoard(BoardDTO boardDTO) { // 게시글 삭제하기
        return boardDAO.deleteBoard(boardDTO);
    }

    @Override
    public int updateBoard(BoardDTO boardDTO) {
        return boardDAO.updateBoard(boardDTO);
    }





    @Override
    public List<BoardReplyDTO> boardReplyList(BoardDTO boardDTO) {
        return boardReplyDAO.boardReplyList(boardDTO);
    }

    @Override
    public int parentReplyInsert(BoardReplyDTO boardReplyDTO) { return boardReplyDAO.parentReplyInsert(boardReplyDTO); }

    @Override
    public int childReplyInsert(BoardReplyDTO boardReplyDTO) { return boardReplyDAO.childReplyInsert(boardReplyDTO); }

    @Override
    public int parentReplyDelete(String no) { return boardReplyDAO.parentReplyDelete(no); }

    @Override
    public int childReplyDelete(String no) { return boardReplyDAO.childReplyDelete(no); }


}






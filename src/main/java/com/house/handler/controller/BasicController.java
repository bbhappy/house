package com.house.handler.controller;

import com.house.handler.dto.MenuAuthDTO;
import com.house.handler.dto.MenuDTO;
import com.house.handler.dto.UserDTO;
import com.house.handler.paging.Criteria;
import com.house.handler.paging.PageMaker;
import com.house.handler.service.MenuService;
import com.house.handler.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@RequestMapping("/basic")
@Controller
public class BasicController extends AdminController{

    @Autowired
    private UserService userService;
    @Autowired
    private MenuService menuService;

    /** 환경설정 - 권한관리 List page 이동 */
    @GetMapping("/auth")
    public ModelAndView userManagementPage(HttpSession session , Criteria cri , HttpServletRequest request) throws Exception{

        //검색 param
        UserDTO userDTO = new UserDTO();
        String search_id =  request.getParameter("search_id");
        String search_nm =  request.getParameter("search_nm");
        userDTO.setUser_id(search_id);
        userDTO.setUser_nm(search_nm);

        //로그인 한사람의 메뉴 및 권한 loading
        String user_id = (String)session.getAttribute("user_id");
        Map<String, List<MenuDTO>> menu = gnbList(user_id);

        //사용자 list loading
        Map<String,Object> map = userService.listUser(cri , userDTO);
        PageMaker pageMaker = new PageMaker(cri,(Integer)map.get("total_count"));

        ModelAndView mv = new ModelAndView();
        mv.addObject("setMenu",menu);
        mv.addObject("totalCount",map.get("total_count"));
        mv.addObject("listUser",map.get("listUser"));
        mv.addObject("pageMaker",pageMaker);
        if (search_id != null) { mv.addObject("search_id",search_id); }
        if (search_nm != null) { mv.addObject("search_nm",search_nm); }
        mv.setViewName("basic/auth");
        return mv;
    }


    /** 환경설정 - 권한설정 page 이동 */
    @GetMapping("/auth_modify")
    public ModelAndView userAuthModifyPage(HttpSession session ,  HttpServletRequest request ) throws Exception{

        //로그인 한사람의 메뉴 및 권한 loading
        String user_id = (String)session.getAttribute("user_id");
        Map<String, List<MenuDTO>> menu = gnbList(user_id);

        //모든 메뉴 리스트 불러오기
        List<MenuDTO> allMenuList = menuService.allMenuList();

        //조회 하려는 사용자의 권한
        List<MenuAuthDTO> userAuthList = menuService.userAuthList(request.getParameter("it_user_id"));

        ModelAndView mv = new ModelAndView();
        mv.addObject("setMenu",menu); 
        mv.addObject("allMenuList",allMenuList);
        mv.addObject("userAuthList",userAuthList);
        mv.addObject("it_user_id",request.getParameter("it_user_id"));
        mv.addObject("it_user_nm",request.getParameter("it_user_nm"));
        mv.setViewName("/basic/auth_modify");
        return mv;
    }


    /** 환경설정 - 권한수정 */
    @ResponseBody
    @PostMapping("/auth_save")
    public String userAuthModify(@RequestParam(value="it_user_id") String it_user_id ,
                                 @RequestParam(value="s_mu_view" , required = false) List<String> s_mu_view ,
                                 @RequestParam(value="s_mu_new" , required = false) List<String> s_mu_new ,
                                 @RequestParam(value="s_mu_mod" , required = false) List<String> s_mu_mod ,
                                 @RequestParam(value="s_mu_del" , required = false) List<String> s_mu_del ) throws Exception{

        //모든 메뉴 순회 하면서 권한 체크 한것 반영하기
        List<MenuDTO> allMenuList = menuService.allMenuList();
        allMenuList.stream().forEach(code->{
            String mu_view = (s_mu_view == null) ? "N" : (s_mu_view.contains(code.getMm_code()) ? "Y" : "N");
            String mu_new = (s_mu_new == null) ? "N" : (s_mu_new.contains(code.getMm_code()) ? "Y" : "N");
            String mu_mod = (s_mu_mod == null) ? "N" : (s_mu_mod.contains(code.getMm_code()) ? "Y" : "N");
            String mu_del = (s_mu_del == null) ? "N" : (s_mu_del.contains(code.getMm_code()) ? "Y" : "N");

            MenuAuthDTO menuAuthDTO = new MenuAuthDTO();
            menuAuthDTO.setEmp_id(it_user_id);
            menuAuthDTO.setMm_parent(code.getMm_parent());
            menuAuthDTO.setMm_code(code.getMm_code());
            menuAuthDTO.setMu_view(mu_view);
            menuAuthDTO.setMu_new(mu_new);
            menuAuthDTO.setMu_mod(mu_mod);
            menuAuthDTO.setMu_del(mu_del);
            menuService.updateMenuAuth(menuAuthDTO);
        });

        return "success";
    }


    /** 환경설정 - 로그인 실패 횟수 초기화 */
    @ResponseBody
    @GetMapping("login_fail_clear")
    public String userLoginFailClear( @RequestParam("it_user_id") String it_user_id ) throws Exception{

        int result = userService.userLoginFailClear( it_user_id );
        if(result > 0){ return "success";
        }else{ return "fail"; }
    }

    /** 환경설정 - 사용자 탈퇴처리 */
    @ResponseBody
    @PostMapping("/user_leave")
    public String userLeave( @RequestParam("checkList") String checkList ) throws Exception{
        if(checkList != "") {
            int result = userService.userLeave(checkList);
            if (result > 0) {
                return "success";
            } else {
                return "fail";
            }
        }else{
            return "fail";
        }
    }

    /** 환경설정 - 선택한 사용자 정보 불러오기 */
    @ResponseBody
    @GetMapping("/user_info")
    public UserDTO userInfo( @RequestParam("it_user_id") String it_user_id ) throws Exception{

        UserDTO userDTO = new UserDTO();
        userDTO.setUser_id(it_user_id);
        UserDTO result = userService.findByUserInfo(userDTO);
        return result;
    }

    /** 환경설정 - 선택한 사용자 정보 수정하기 */
    @ResponseBody
    @PostMapping("/user_info_modify")
    public String userInfoModify( @ModelAttribute("userVo") UserDTO userDTO) throws Exception{

        int result = userService.userInfoModify(userDTO);
        if(result > 0) {
            userDTO.setFail_count(0);
            userService.updateFailCount( userDTO );// 시도횟수 0
            return "ok";
        } else { return "fail"; }
    }



}

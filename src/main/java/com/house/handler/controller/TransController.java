package com.house.handler.controller;

import com.house.handler.dto.MenuDTO;
import com.house.handler.service.MenuService;
import com.house.handler.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@Controller
public class TransController extends AdminController{

    @Autowired
    private UserService userService;
    @Autowired
    private MenuService menuService;

    /** PID 관리 - PID-1 page 이동 */
    @RequestMapping("/trans/pid1")
    public ModelAndView pidPage1( HttpSession session ){

        String user_id = (String)session.getAttribute("user_id");
        Map<String, List<MenuDTO>> menu = gnbList(user_id);

        ModelAndView mv = new ModelAndView();
        mv.addObject("setMenu",menu);
        mv.setViewName("/trans/pid1");
        return mv;
    }

    /** PID 관리 - PID-2 page 이동 */
    @RequestMapping("/trans/pid2")
    public ModelAndView pidPage2( HttpSession session ){

        String user_id = (String)session.getAttribute("user_id");
        Map<String, List<MenuDTO>> menu = gnbList(user_id);

        ModelAndView mv = new ModelAndView();
        mv.addObject("setMenu",menu);
        mv.setViewName("/trans/pid2");
        return mv;
    }

}

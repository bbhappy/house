package com.house.handler.controller;

import com.house.handler.dto.UserDTO;
import com.house.handler.service.UserService;
import com.house.handler.util.SHA256;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Controller
public class LoginController extends AdminController {

    @Autowired
    private UserService userService;

    /** login page */
    @RequestMapping("/")
    public String index(HttpSession session) throws Exception{
        if(session.getAttribute("user_id") != null) { //이미 세션이 있는경우 세션 삭제
            session.invalidate();
        }
        return "login/login";
    }

    /** 회원가입 페이지 이동 */
    @GetMapping("/login/signup")
    public String signup(){
        return "login/signup";
    }


    /** 회원 가입시 이메일 인증번호 보내기
     *  signup.js */
    @ResponseBody
    @GetMapping("/login/checkEmail")
    public String checkEmail(@RequestParam(value="email") String email) throws Exception{

        UserDTO userDTO = new UserDTO();
        userDTO.setCommand("mailCheck");
        userDTO.setUser_email(email);
        if(userService.checkUserDuplicated( userDTO )) { // 사용자 중복확인
            return "duplicatedMail";
        }

        Random random = new Random();
        int checkNum = random.nextInt(888888) + 111111;

        /* 이메일 보내기 */
        String setFrom = "backbbcheck@gmail.com";
        String toMail = email;
        String title = "다정다감 회원가입 인증메일 입니다.";
        String content = "홈페이지를 방문해주셔서 감사합니다.<br><br>" +
                         "인증 번호는 " + checkNum + "입니다.<br>" +
                         "해당 인증번호를 인증번호 확인란에 기입하여 주세요.";
        String result = AuthMailSend( setFrom, toMail, title, content );
        if(result == "ok") {
            return Integer.toString(checkNum);
        }else{  return ""; }
    }


    /** 회원가입 등록
     * signup.js*/
    @ResponseBody
    @PostMapping("/login/userInfo")
    public Map<String, Object> userInfo( @ModelAttribute("userVO") UserDTO userDTO ) throws Exception {

        String salt = SHA256.generateSalt(); //솔트 값 발생
        String pw = userDTO.getUser_pw();
        pw = SHA256.getEncrypt(pw, salt);

        userDTO.setUser_pw(pw);
        userDTO.setUser_salt(salt);
        userDTO.setReq_dt( new Date() );
        userDTO.setModify_pw( new Date() );

        Map<String, Object> resultMap = new HashMap<>();
        try{
            userDTO.setCommand("idCheck");
            if(userService.checkUserDuplicated( userDTO )) { // 사용자 중복확인
                resultMap.put("status","duplicatedId");
                return resultMap;
            }

            userDTO.setCommand("mailCheck");
            if(userService.checkUserDuplicated( userDTO )) { // 사용자 중복확인
                resultMap.put("status","duplicatedMail");
                return resultMap;
            }

            userService.insert(userDTO); // 사용자 등록
            resultMap.put("status", "ok");
            return resultMap;

        }catch(Exception e){
            resultMap.put("status",e.getMessage());
            return resultMap;
        }
    }


    /** 아이디 찾기page */
    @GetMapping("/login/findId")
    public String findId() throws Exception {
        return "login/find_id";
    }


    /** 이메일 입력 후 id 찾기 진행
     * find_id.js */
    @ResponseBody
    @GetMapping("/login/findIdByEmail")
    public String findIdByEmail( @RequestParam(value="email") String email ) throws Exception{

        UserDTO userDTO = new UserDTO();
        userDTO.setUser_email(email);
        userDTO.setCommand("mailCheck");

        if(userService.checkUserDuplicated( userDTO )) {
            /* 이메일 보내기 */
            UserDTO info = userService.findByUserInfo(userDTO);
            String setFrom = "backbbcheck@gmail.com";
            String toMail = userDTO.getUser_email();
            String title = "다정다감 사용자 ID 찾기";
            String content =  "<br><br>다정다감 ID는 " + info.getUser_id() + "입니다.";
            System.out.println("3 =========>");

            return AuthMailSend( setFrom, toMail, title, content );
        }else{
            return "no_exist";
        }
    }


    /** 비밀번호 찾기page */
    @GetMapping("/login/findPw")
    public String findPw() throws Exception {
        return "login/find_pw";
    }


    /** 비밀번호 찾기 id, 이메일 입력 후 인증번호 발송
     * find_pw.js */
    @ResponseBody
    @GetMapping("/login/findPwByEmail")
    public String findPwByEmail( @RequestParam(value="user_id") String user_id ,
                                 @RequestParam(value="user_email") String user_email ) throws Exception {

        UserDTO userDTO = new UserDTO();
        userDTO.setUser_id(user_id);
        userDTO.setUser_email(user_email);
        userDTO.setCommand("idAndMailCheck");

        if(userService.checkUserDuplicated( userDTO )) {
            Random random = new Random();
            int checkNum = random.nextInt(888888) + 111111;

            /* 이메일 보내기 */
            String setFrom = "backbbcheck@gmail.com";
            String toMail = userDTO.getUser_email();
            String title = "다정다감 비밀번호 찾기 인증메일 입니다.";
            String content = "홈페이지를 방문해주셔서 감사합니다.<br><br>" +
                    "인증 번호는 " + checkNum + "입니다.<br>" +
                    "해당 인증번호를 인증번호 확인란에 기입하여 주세요.";
            String result = AuthMailSend( setFrom, toMail, title, content );
            if(result == "ok") { return Integer.toString(checkNum);
            }else{  return ""; }
        }else{
            return "no_exist";
        }
    }


    /** 비밀번호 변경 page */
    @PostMapping("/login/pwChangePage")
    public String pwChangePage(HttpServletRequest request , Model model) throws Exception {
        String check_num = request.getParameter("check_num");
        String compare_num = request.getParameter("compare_num");

        if(check_num.equals(compare_num)) {
            model.addAttribute("user_id",request.getParameter("user_id"));
            model.addAttribute("user_email",request.getParameter("user_email"));
            return "login/change_pw";
        }else{
            return "login/find_pw";
        }
    }


    /** 비밀번호 변경 처리 */
    @ResponseBody
    @PostMapping("/login/changePw")
    public String changePw( @ModelAttribute("userVo") UserDTO userDTO ) throws Exception {

        if(userService.checkUserDuplicated( userDTO )) {

            String salt = SHA256.generateSalt(); //솔트 값 발생
            String pw = userDTO.getUser_pw();
            pw = SHA256.getEncrypt(pw, salt);

            userDTO.setUser_pw(pw);
            userDTO.setModify_pw( new Date() );
            userDTO.setUser_salt(salt);

            int result = userService.updateNewPw(userDTO);
            if(result > 0) {
                userDTO.setFail_count(0);
                userService.updateFailCount( userDTO );// 시도횟수 0
                return "ok";
            } else { return "fail"; }

        }else{
            return "no_exist";
        }
    }


    /** 로그인 버튼 click */
    @ResponseBody
    @RequestMapping("/login/start")
    public Map<String, Object> login(HttpSession session , @ModelAttribute("userVO") UserDTO userDTO ) throws Exception {

        Map<String, Object> resultMap = new HashMap<>();
        try{
            userDTO.setCommand("idCheck");
            if( userService.checkUserDuplicated( userDTO ) ){ // 사용자 중복확인

                UserDTO findUserVO = userService.findByUserInfo(userDTO);
                String d_salt = userService.getSalt(userDTO.getUser_id());
                String input_pw = SHA256.getEncrypt(userDTO.getUser_pw(), d_salt);

                if(findUserVO.getFail_count() < 5) { // 로그인 5회연속 실패시 접속 불가능

                    if (findUserVO.getUser_pw().equals(input_pw)) { // pw 확인
                        session.setAttribute("user_id", findUserVO.getUser_id());
                        session.setAttribute("user_nm", findUserVO.getUser_nm());

                        findUserVO.setFail_count(0);
                        userService.updateFailCount( findUserVO ); // 시도횟수 초기화
                        resultMap.put("status", "success");
                    } else {
                        findUserVO.setFail_count(findUserVO.getFail_count() + 1);
                        userService.updateFailCount( findUserVO );// 시도횟수 +1

                        resultMap.put("trial", findUserVO.getFail_count());
                        resultMap.put("status", "pw_check");
                    }
                }else{
                    resultMap.put("status", "trial_fail");
                }
            }else{
                resultMap.put("status", "no_user");
            }
        }catch(Exception e){
            resultMap.put("status",e.getMessage());
            return resultMap;
        }
        return resultMap;
    }

}

package com.house.handler.controller;

import com.house.handler.dto.MenuDTO;
import com.house.handler.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServlet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class AdminController extends HttpServlet {

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private MenuService menuService;

    /** 메뉴 */
    public Map<String, List<MenuDTO>> gnbList(String user_id ){

        Map<String,List<MenuDTO>> menuList = new LinkedHashMap ();
        List<MenuDTO> rootMenu = menuService.getAdminMenu();
        rootMenu.stream().map(p -> p.getMm_parent()).forEach(
                p -> {
                    Map<String, Object> map = new LinkedHashMap();
                    map.put("mm_parent", p);
                    map.put("user_id", user_id);
                    List<MenuDTO> menu = menuService.menuAuthList(map);
                    menuList.put(p,menu);
                }
        );
        return menuList;
    }


    /**  이메일로 인증번호 메일보내기 */
    public String AuthMailSend( String setFrom, String toMail, String title, String content){
        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");
            helper.setFrom(setFrom);
            helper.setTo(toMail);
            helper.setSubject(title);
            helper.setText(content,true);
            mailSender.send(message);
            return "ok";
        }catch(Exception e) {
            e.printStackTrace();
            return "error";
        }
    }

}

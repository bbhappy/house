
package com.house.handler.controller;

import com.google.gson.JsonObject;
import com.house.handler.dto.BoardDTO;
import com.house.handler.dto.BoardReplyDTO;
import com.house.handler.dto.MenuDTO;
import com.house.handler.paging.Criteria;
import com.house.handler.paging.PageMaker;
import com.house.handler.service.BoardService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.*;

@RequestMapping("/board")
@Controller
public class BoardController extends AdminController{

    @Autowired
    private BoardService boardService;

    /** 게시판 - 자유게시판 page 이동 */
    @GetMapping("/listPage")
    public ModelAndView boardPage(HttpSession session , Criteria cri , HttpServletRequest request ) throws Exception{

        BoardDTO boardDTO = new BoardDTO();                                         // 검색 param
        String search_nm = request.getParameter("search_nm");
        String search_title = request.getParameter("search_title");
        boardDTO.setWriter(search_nm);
        boardDTO.setTitle(search_title);

        String user_id = (String)session.getAttribute("user_id");
        Map<String, List<MenuDTO>> menu = gnbList(user_id);                          // 로그인 한사람의 메뉴 및 권한 loading

        Map<String,Object> map = boardService.listBoard(cri , boardDTO);             // 사용자 list loading
        PageMaker pageMaker = new PageMaker(cri,(Integer)map.get("total_count"));

        ModelAndView mv = new ModelAndView();
        mv.addObject("setMenu",menu);
        mv.addObject("totalCount",map.get("total_count"));
        mv.addObject("listBoard",map.get("listBoard"));
        mv.addObject("pageMaker",pageMaker);

        //search
        if (search_nm != null) { mv.addObject("search_nm",search_nm); }
        if (search_title != null) { mv.addObject("search_content",search_title); }
        mv.setViewName("board/list");
        return mv;
    }


    /** 게시판 - 자유게시판 쓰기 page 이동 */
    @RequestMapping("/writePage")
    public ModelAndView boardWrite(HttpSession session) throws  Exception{

        String user_id = (String)session.getAttribute("user_id");           // 로그인 한사람의 메뉴 및 권한 loading
        Map<String, List<MenuDTO>> menu = gnbList(user_id);

        ModelAndView mv = new ModelAndView();
        mv.addObject("setMenu",menu);
        mv.setViewName("/board/write");
        return mv;
    }


    /** 게시판 - 자유게시판 쓰기 :: 서머노트 이미지 파일 저장 */
    @RequestMapping(value="/uploadSummernoteImageFile", produces = "application/json; charset=utf8")
    @ResponseBody
    public String uploadSummernoteImageFile( @RequestParam("file") MultipartFile multipartFile, HttpServletRequest request )  throws Exception{
        JsonObject jsonObject = new JsonObject();

        /* String fileRoot = "C:\\summernote_image\\";                                                     // 외부경로로 저장을 희망할때. */
        String fileRoot = request.getServletContext().getRealPath("resources/images/board/");            // 내부경로로 저장
        String originalFileName = multipartFile.getOriginalFilename();                                     // 오리지널 파일명
        String extension = originalFileName.substring(originalFileName.lastIndexOf("."));              // 파일 확장자

        final String[] ALLOW_EXTENSION = {".gif",".GIF", ".jpg",".JPG",".png",".PNG",".jepg",".JEPG"};      // 확장자 검사
        if(!Arrays.asList(ALLOW_EXTENSION).contains(extension)){
            jsonObject.addProperty("responseCode", "extension");
            String a = jsonObject.toString();
            return a;
        }

        String savedFileName = UUID.randomUUID() + extension;  // 파일 이름이 한글로 들어왔을 때 그걸 다시 영어와 숫자로 이루어진 문자열로 만들기 UUTID
        File targetFile = new File(fileRoot + savedFileName);
        try {
            InputStream fileStream = multipartFile.getInputStream();
            FileUtils.copyInputStreamToFile(fileStream, targetFile);

            // 파일 저장
            jsonObject.addProperty("url", "/resources/images/board/"+ savedFileName); // contextroot + resources + 저장할 내부 폴더명
            jsonObject.addProperty("responseCode", "success");

        } catch (IOException e) {
            FileUtils.deleteQuietly(targetFile);   // 저장된 파일 삭제
            jsonObject.addProperty("responseCode", "error");
            e.printStackTrace();
        }
        String a = jsonObject.toString();
        return a;
    }


    /** 게시판 - 자유게시판 쓰기 ::  글 저장 */
    @ResponseBody
    @PostMapping("/insertBoard")
    public String insertBoard( HttpSession session, @ModelAttribute("boardVo") BoardDTO boardDTO) throws Exception{
        String user_id = (String)session.getAttribute("user_id");
        String user_nm = (String)session.getAttribute("user_nm");
        boardDTO.setWriter(user_nm);
        boardDTO.setWriter_id(user_id);
        int idx = boardService.insertBoard(boardDTO);                         // write transaction

        if(idx > 0){ return "success"; }
        else{ return "fail"; }
    }

    /** 게시판 - 자유게시판 글 읽기 */
    @GetMapping("/view")
    public ModelAndView viewBoard( HttpSession session, @RequestParam(required=false, value="chk_board_no") int chk_board_no ) throws Exception{

        String user_id = (String)session.getAttribute("user_id");
        Map<String, List<MenuDTO>> menu = gnbList(user_id);                         // 로그인 한사람의 메뉴 및 권한 loading

        BoardDTO boardDTO = new BoardDTO();
        boardDTO.setIdx(chk_board_no);
        boardService.boardViewUpdate(boardDTO);                                     // 조회수 +1
        BoardDTO boardList = boardService.searchBoardOne(boardDTO);                 // content data loading

        ModelAndView mv = new ModelAndView();
        mv.addObject("setMenu",menu);
        mv.addObject("boardList",boardList);
        mv.setViewName("/board/view");
        return mv;
    }

    /** 게시판 - 자유게시판 글 수정 page 이동 */
    @RequestMapping("/modifyPage")
    public ModelAndView modifyPage(HttpSession session , @RequestParam(required=false, value="chk_board_no") int chk_board_no) throws  Exception{

        String user_id = (String)session.getAttribute("user_id");   // 로그인 한사람의 메뉴 및 권한 loading
        Map<String, List<MenuDTO>> menu = gnbList(user_id);

        BoardDTO boardDTO = new BoardDTO();
        boardDTO.setIdx(chk_board_no);
        BoardDTO boardList = boardService.searchBoardOne(boardDTO);   // content data loading

        ModelAndView mv = new ModelAndView();
        mv.addObject("setMenu",menu);
        mv.addObject("boardList",boardList);
        mv.setViewName("/board/modify");
        return mv;
    }

    /** 게시판 - 자유게시판 글 삭제 */
    @ResponseBody
    @GetMapping("/boardDelete")
    public String modifyPage( @RequestParam(required=false, value="chk_board_no") int chk_board_no) throws  Exception{

        BoardDTO boardDTO = new BoardDTO();
        boardDTO.setIdx(chk_board_no);
        int result = boardService.deleteBoard(boardDTO);            // delete transaction
        if(result > 0){ return "success"; }
        else{ return "fail"; }
    }

    /** 게시판 - 자유게시판 수정 ::  글 수정 */
    @ResponseBody
    @PostMapping("/updateBoard")
    public String updateBoard( @ModelAttribute("boardVo") BoardDTO boardDTO ) throws Exception{

        LocalDateTime currentDateTime = LocalDateTime.now();        // write time
        boardDTO.setReq_dt(currentDateTime);

        int idx = boardService.updateBoard(boardDTO);
        if(idx > 0){ return "success"; }
        else{ return "fail"; }
    }


    /** 게시판 - 자유게시판 댓글리스트 */
    @ResponseBody
    @GetMapping("/replyList")
    public Map<String,Object> boardReplyList( @RequestParam(required=false, value="chk_board_no") int chk_board_no ) throws Exception{

        BoardDTO boardDTO = new BoardDTO();
        boardDTO.setIdx(chk_board_no);
        List<BoardReplyDTO> boardReplyList = boardService.boardReplyList(boardDTO); // content reply loading

        Map<String,Object> map = new HashMap<>();
        map.put("boardReplyList",boardReplyList);
        map.put("result","success");
        return map;
    }

    /** 게시판 - 자유게시판 댓글 저장 */
    @ResponseBody
    @GetMapping("/replyInsert")
    public String replyInsert( HttpSession session,
                               @RequestParam(required = false , value = "mode") String mode,
                               @ModelAttribute("boardReplyVO") BoardReplyDTO boardReplyDTO ) throws Exception{

        boardReplyDTO.setWriter((String)session.getAttribute("user_id"));

        int result = 0;
        if(mode.equals("parent")){ result = boardService.parentReplyInsert(boardReplyDTO); }
        else{ result = boardService.childReplyInsert(boardReplyDTO); }

        if(result > 0) return "success";
        else return "error";
    }

    /** 게시판 - 자유게시판 댓글 삭제 */
    @ResponseBody
    @GetMapping("/replyDelete")
    public String replyDelete( @RequestParam("no") String no, @RequestParam("grp") String grp ){

        int result = 0;
        if (no.equals(grp)) result = boardService.parentReplyDelete(no);
        else result = boardService.childReplyDelete(no);
        if(result > 0) return "success";
        else return "error";
    }


}

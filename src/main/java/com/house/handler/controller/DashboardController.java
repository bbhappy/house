package com.house.handler.controller;

import com.house.handler.dto.MenuDTO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@Controller
public class DashboardController extends AdminController {

    /** 대시보드 */
    @RequestMapping("dashboard")
    public ModelAndView dashboard(HttpSession session) throws Exception{

        //로그인 한사람의 메뉴 및 권한 loading
        String user_id = (String)session.getAttribute("user_id");
        Map<String, List<MenuDTO>> menu = gnbList(user_id);

        ModelAndView mv = new ModelAndView();
        mv.addObject("setMenu",menu);
        mv.setViewName("dashboard");
        return mv;
    }


}

<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, shrink-to-fit=no">
    <meta http-equiv="imagetoolbar" content="no" />
    <meta http-equiv="Cache-Control" content="No-Cache" />
    <meta HTTP-EQUIV="Pragma" CONTENT="no-cache"/>
    <meta HTTP-EQUIV="Expires" CONTENT="-1"/>

    <%@ page contentType="text/html;charset=utf-8" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

    <!-- 위 3개의 메타 태그는 *반드시* head 태그의 처음에 와야합니다; 어떤 다른 콘텐츠들은 반드시 이 태그들 *다음에* 와야 합니다 -->
    <title>다정다감</title>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>

    <!--font-->
    <link rel="stylesheet" type="text/css" href="https://cdn.rawgit.com/moonspam/NanumSquare/master/nanumsquare.css">
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+KR:100,300,400,500,700,900&display=swap" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe" crossorigin="anonymous"></script>

    <script src="<c:url value="/resources/js/common/jquery-3.4.1.min.js" />"></script>

    <!-- CSS , JS -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="<c:url value="/resources/css/admin/bootstrap.custom.css?v2r" />">
    <link rel="stylesheet" href="<c:url value="/resources/css/admin/bootstrap.ie.css?v2" />">

    <!-- 기본 CSS -->
    <link rel="stylesheet" href="<c:url value="/resources/css/admin/common.css" />">

    <!-- View -->
    <script src="<c:url value="/resources/js/common/common.js" />"></script>
    <script src="<c:url value="/resources/js/common/common2.js" />"></script>

    <!-- Number js -->
    <script type="text/javascript" src="<c:url value="/resources/js/common/jquery.number.js" />"></script>

    <!-- Form Validation -->
    <script type="text/javascript" src="<c:url value="/resources/js/common/jquery.validate.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/common/jquery.validate.add-method.js" />"></script>

    <!-- Toast Ui -->
    <link rel="stylesheet" href="https://uicdn.toast.com/chart/latest/toastui-chart.min.css" />
    <script src="https://uicdn.toast.com/chart/latest/toastui-chart.min.js"></script>
    <script src="https://unpkg.com/babel-standalone@6.26.0/babel.min.js"></script>

    <!-- Select2 -->
    <script type="text/javascript" src="<c:url value="/resources/js/common/select2.min.js" />"></script>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/admin/select2.min.css"/>">

    <!-- bootstrap-toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>


    <script>
        $(document).ready(function(){
            $(".parent>a").click(function(){
                $(".site-nav ul li").removeClass("active");
                $(".site-nav ul ul").slideUp();

                $(this).closest(".parent").addClass("active");
                $(this).closest(".parent").next("ul").slideToggle();
            });
        });
    </script>

</head>
<body>
<!-- navbar -->
<nav class="site-nav navbar navbar-expand-md navbar-light">
    <div class="container-fluid">
        <div class="navbar-header">
            <button class="navbar-toggler ml-auto mb-2 bg-light" type="button" data-toggle="collapse" data-target="#myNavbar" >
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <div class="row">
                <!-- sidebar -->
                <div class="col-xl-2 col-lg-3 col-md-4 sidebar fixed-top">
                    <button class="navbar-toggler ml-auto mt-2 bg-light" type="button" data-toggle="collapse" data-target="#myNavbar" >
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <a href="../dashboard" class="navbar-brand text-white d-block mx-auto text-center py-3 mb-4 bottom-border">Your DID System</a>
                    <div class="bottom-border pb-3 mt-2">
                        <span class="text-white"><c:out value="${sessionScope.user_nm}" /> 님</span>
                        <a href="/" class="text-primary ml-2">로그아웃</a>
                    </div>
                    <ul class="navbar-nav flex-column mt-4">
                        <c:forEach items="${setMenu}" var="menu" varStatus="status">
                            <c:set var="sub_code" value="" /><%-- 하위menu key 초기값 --%>
                            <c:if test="${not empty menu.key}" >

                                <!--- 메뉴 그리기 Start -->
                                <c:forEach items="${menu.value}" var="dto">

                                    <c:set var="req_path" value="${requestScope['javax.servlet.forward.servlet_path']}" />
                                    <c:if test="${req_path eq dto.mm_path}"><!--- 각 메뉴 jsp view 접근 권한 체크 Start -->
                                       <c:choose>
                                           <c:when test="${dto.menuAuth.mu_view ne 'Y' }">
                                               <c:redirect url="/dashboard" />
                                           </c:when>
                                           <c:when test="${dto.menuAuth.mu_view eq 'Y' }">
                                               <c:set var="u_menu_navi" value="${dto.mm_navi}" /><!--- 메뉴 이름 -->
                                               <c:set var="u_auth_new" value="${dto.menuAuth.mu_new}" /><!--- 각 메뉴 jsp 접근 권한 loading -->
                                               <c:set var="u_auth_mod" value="${dto.menuAuth.mu_mod}" />
                                               <c:set var="u_auth_del" value="${dto.menuAuth.mu_del}" />
                                           </c:when>
                                       </c:choose>
                                    </c:if>

                                    <c:if test="${dto.menuAuth.mu_view eq 'Y' }" ><!-- 메뉴별 jsp view 접근 권한 없는경우 메뉴 보여주지 않음 -->

                                        <c:if test="${menu.key eq dto.mm_parent}" >
                                            <c:set var="sub_code" value="${dto.mm_code}" /><!-- 하위 menu compare key set -->
                                            <li class="nav-item parent">
                                                <a href="#" class="nav-link text-white p-3 sidebar-link">
                                                    ${dto.mm_icon}${dto.mm_module}<!-- 대 menu -->
                                                </a>
                                            </li>
                                             <ul class="hide">
                                        </c:if>
                                        <c:if test="${sub_code eq dto.mm_parent}">
                                            <li><a href="${dto.mm_path}" class="text-white">${dto.mm_name}</a></li><!-- 하위 menu -->
                                        </c:if>

                                    </c:if>
                                </c:forEach>
                                <c:if test="${not empty sub_code}">
                                    </ul>
                                    </li>
                                </c:if>
                                <!--- 메뉴 그리기 End -->

                            </c:if>
                        </c:forEach>
                    </ul>
                </div>
                <!-- end of sidebar -->
            </div>
        </div>
    </div>
</nav>
<!-- end of navbar -->



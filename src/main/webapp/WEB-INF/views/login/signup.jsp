<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.net.URLDecoder"%>

<%@ include file="../inc/loginHeader.jsp" %>
<link href="<c:url value="/resources/css/login/signup.css" />" rel="stylesheet">
<script type="text/javascript" src="<c:url value="/resources/js/login/signup.js" />"></script>

</head>
<body>

<div class="wrapper">
    <div class="login" >
        <form id="userInfoFrm" >
            <div class="title">사용자 등록</div>
            <div id="msg" class="msg">
                <c:if test="${not empty param.msg}">
                    <i class="fa fa-exclamation-circle"> ${URLDecoder.decode(param.msg)}</i>
                </c:if>
            </div>
            <input class="input-info form-control" type="text" name="user_id" id="user_id" placeholder="아이디 : 3자리 이상" maxlength="15">
            <input class="input-info form-control" type="password" name="user_pw" id="user_pw" maxlength="10"  placeholder="비밀번호 : 5~8자리 영어, 숫자, (!@$%^&*) 조합">
            <input class="input-info form-control" type="text" name="user_nm" id="user_nm" placeholder="이름 : 홍길동" maxlength="15">
            <input class="input-info form-control phoneNumber" type="tel" name="user_tel" id="user_tel" placeholder="연락처 (띄어쓰기 없이 숫자만 입력)" maxlength="15">
            <input class="input-info form-control" type="email" name="user_email" id="user_email" placeholder="비밀번호를 찾기위한 이메일" maxlength="30" onkeyup="keyClear()" >
            <input class="input-info form-control" type="number" name="check_num" id="check_num" placeholder="이메일에 적힌 인증번호" maxlength="30" min="0">
            <input class="input-info form-control" type="hidden" name="compare_num" id="compare_num">
        </form>
        <div class="btn-group col-xl-12 col-md-12 col-sm-12" role="group">
            <button class="btn btn-lg btn-info mr-1" onclick="mailCheck()">메일인증</button>
            <button class="btn btn-lg btn-success mr-1" onclick="formCheck()">등록</button>
            <button class="btn btn-lg btn-danger" onclick="history.back(-1)">뒤로가기</button>
        </div>
    </div>
</div>

<%@ include file="../inc/loginFooter.jsp" %>
</body>
</html>



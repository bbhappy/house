<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="../inc/loginHeader.jsp" %>
<link href="<c:url value="/resources/css/login/signup.css" />" rel="stylesheet">
<script type="text/javascript" src="<c:url value="/resources/js/login/find_pw.js" />" ></script>

</head>
<body>

<div class="wrapper">
    <div class="login" >
        <div class="title">비밀번호 찾기</div>
        <div id="msg" class="msg">
            <i class="fa fa-exclamation-circle"> 등록된 이메일로 인증번호를 전송합니다.</i>
        </div>
        <form id="frm" action="/login/pwChangePage" method="post">
            <input class="input-info form-control" type="text" name="user_id" id="user_id" placeholder="1. 사용자ID" maxlength="15">
            <input class="input-info form-control" type="email" name="user_email" id="user_email" placeholder="2. 등록된 이메일" maxlength="30" onkeyup="keyClear()" >
            <input class="input-info form-control" type="number" name="check_num" id="check_num" placeholder="3. 이메일에 적힌 인증번호" maxlength="30" min="0">
            <input class="input-info" type="hidden" name="compare_num" id="compare_num">
        </form>

        <div class="btn-group col-xl-12 col-md-12 col-sm-12" role="group">
            <button class="btn btn-default btn-md btn-info mr-1" onclick="mailKeySend()">인증번호받기</button>
            <button class="btn btn-default btn-md btn-info mr-1" onclick="pwFind()">비밀번호찾기</button>
            <button class="btn btn-default btn-md btn-info" onclick="history.back(-1)">뒤로가기</button>
        </div>
    </div>
</div>

<%@ include file="../inc/loginFooter.jsp" %>
</body>
</html>

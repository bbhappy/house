<%@ page contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<%@ include file="../inc/adminHeader.jsp" %>
<script type="text/javascript" src="<c:url value="/resources/js/basic/auth.js" />" ></script>

<section>
    <div class="col-xl-10 col-lg-9 col-md-8 ml-auto bg-light fixed-top py-2 top-navbar">
        <div class="row align-items-center">
            <div class="col-md-4">
                <h4 class="text-secondary text-uppercase mb-2 mt-2">${u_menu_navi}</h4>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-10 col-md-8 col-lg-9 ml-auto">
                <div class="row pt-md-5 mt-md-3 mb-5">
                    <div class="col-xl-12 col-md-12 col-sm-12 p2 mt-5">

                        <form name="requestForm" id="requestForm" method="post">
                            <input type="hidden" name="checkList" id="checkList" value="">
                            <div class="row justify-content-between col-xl-12 col-md-12 col-sm-12 mb-2">
                                <div class="px-3">
                                    <span id="searchDetailLabel" data-toggle="dropdown" class="btn btn-sm btn-outline-primary dropdown-toggle"><i class="fas fa-search"></i></span>
                                    <div class="dropdown-menu px-2 container-fluid" aria-labelledby="searchDetailLabel" style="width:70%">
                                        <div class="form-row mb-2 ml-2">
                                            <div class="col-sm-12">
                                                <div class="input-group input-group-lg pt-1">
                                                    <div class="input-group-prepend"><span class="input-group-text">이&nbsp;&nbsp;&nbsp;름</span></div>
                                                    <input type="text" name="search_nm" id="search_nm" value="${search_nm}" onkeyup="javascript:if(event.keyCode == 13){searchDetail();}" class="form-control" aria-label="이름" aria-describedby="이름">
                                                </div>
                                                <div class="input-group input-group-lg pt-1">
                                                    <div class="input-group-prepend"><span class="input-group-text">아이디</span></div>
                                                    <input type="text" name="search_id" id="search_id" value="${search_id}" onkeyup="javascript:if(event.keyCode == 13){searchDetail();}" class="form-control" aria-label="아이디" aria-describedby="아이디">
                                                </div>
                                            </div>
                                        </div>
                                        <a type="button" class="btn btn-sm btn-outline-primary btn-block active" onclick="searchDetail();" aria-label="검색"> 검색 </a>
                                    </div>
                                </div>
                                <c:if test="${u_auth_del eq 'Y'}" >
                                <button type="button" class="btn btn-sm btn-outline-danger" onclick="btnDeleteCheckBox()">선택한 사용자 탈퇴</button>
                                </c:if>
                            </div>
                            <table class="table table-sm  table-bordered table-hover" id="userTable">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col" class="text-center">
                                            <input type="checkbox" id="allCheck" name="allCheck" />
                                        </th>
                                        <th scope="col" class="text-center">이름 (ID)</th>
                                        <th scope="col" class="text-center d-none d-lg-table-cell">이메일</th>
                                        <th scope="col" class="text-center d-none d-lg-table-cell">연락처</th>
                                        <th scope="col" class="text-center d-none d-lg-table-cell">암호 변경일</th>
                                        <th scope="col" class="text-center">권한설정</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:if test="${totalCount gt 0 }">
                                    <c:forEach items="${listUser}" var="user" >
                                        <tr class="text-align bg-white" style="cursor:pointer">
                                            <td class="text-center">
                                                <input type="checkbox" id="chk_it_no" name="chk_it_no" value="${user.getUser_no()}" />
                                                <input type="hidden" id="chk_user_nm" name="chk_user_nm" value="${user.getUser_nm()}" />
                                                <input type="hidden" id="chk_user_id" name="chk_user_id" value="${user.getUser_id()}" />
                                            </td>
                                            <td class="<c:if test="${u_auth_mod eq 'Y'}" >rowTr</c:if>" >
                                                ${user.getUser_nm()}
                                                <span style="color: cadetblue"> ( ${user.getUser_id()} ) </span>
                                            </td>
                                            <td class="<c:if test="${u_auth_mod eq 'Y'}" >rowTr</c:if> d-none d-lg-table-cell">
                                                ${user.getUser_email()}
                                            </td>
                                            <td class="<c:if test="${u_auth_mod eq 'Y'}" >rowTr</c:if> d-none d-lg-table-cell">
                                                ${user.getUser_tel()}
                                            </td>
                                            <td class="rowTr d-none d-lg-table-cell">

                                                <fmt:formatDate value='${user.getModify_pw()}' pattern='yyyy-MM-dd' var="modiDate"/>
                                                <fmt:parseDate value="${modiDate}" var="modiDate2" pattern="yyyy-MM-dd"/>
                                                <fmt:parseNumber value="${modiDate2.time / (1000*60*60*24)}" integerOnly="true" var="modiDate3"/>

                                                <jsp:useBean id="today" class="java.util.Date" />
                                                <fmt:formatDate value='${today}' pattern='yyyy-MM-dd' var="nowDate"/>
                                                <fmt:parseDate value="${nowDate}" var="nowDate2" pattern="yyyy-MM-dd"/>
                                                <fmt:parseNumber value="${nowDate2.time / (1000*60*60*24)}" integerOnly="true" var="nowDate3"/>
                                                <c:set var="check_pw" value="${nowDate3 - modiDate3}" />

                                                ${modiDate}
                                                <c:if test="${check_pw gt 30}" >
                                                    <span class="ml-3" style="font-family: 'AmeriGarmnd BT';color:brown"><b>30일 초과</b></span>
                                                </c:if>
                                            </td>
                                            <td>
                                                <c:if test="${u_auth_new eq 'Y'}" >
                                                    <button type="button" class="btn btn-sm btn-outline-info btnGrant">권한설정</button>
                                                    <button type="button" class="btn btn-sm btn-outline-danger d-none d-lg-table-cell btnPwClear">접속실패 ${user.getFail_count()}회</button>
                                                </c:if>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </c:if>
                                <c:if test="${totalCount eq 0 }">
                                    <tr class="center">
                                        <td scope='row' colspan='10'>검색 결과가 없습니다.</td>
                                    </tr>
                                </c:if>
                                </tbody>
                            </table>
                            <div style="margin-left:38%">
                            <ul class="pagination">
                                <c:if test="${pageMaker.prev }">
                                    <li class="page-item"><a class="page-link" href="<c:url value="/basic/auth?pageNum=1"/>">맨앞</a></li>
                                </c:if>
                                <c:if test="${pageMaker.prev }">
                                    <li class="page-item">
                                        <a class="page-link" href="<c:url value="/basic/auth?pageNum=${pageMaker.cri.pageNum-1}"/>">이전</a>
                                    </li>
                                </c:if>
                                <c:if test="${pageMaker.endPage gt 1}">
                                    <c:forEach var="num" begin="${pageMaker.startPage }" end="${pageMaker.endPage }">
                                        <li class="page-item ${pageMaker.cri.pageNum == num? "active":"" }">
                                            <a class="page-link" href=" <c:url value="/basic/auth?pageNum=${num}"/>">${num}</a>
                                        </li>
                                    </c:forEach>
                                </c:if>
                                <c:if test="${pageMaker.next }">
                                    <li class="page-item"><a class="page-link" href="<c:url value="/basic/auth?pageNum=${pageMaker.cri.pageNum+1}"/>">다음</a></li>
                                </c:if>
                                <c:if test="${pageMaker.next }">
                                    <li class="page-item"><a class="page-link" href="<c:url value="/basic/auth?pageNum=${pageMaker.lastPage}"/>">맨끝</a></li>
                                </c:if>
                            </ul>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- 사용자 정보 수정: modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="userModal" data-backdrop='static' >
    <div class="modal-dialog modal-sm" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">사용자 정보 수정</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-xl-12 col-sm-12 ">
                    <form name="userForm" id="userForm" method="post">
                        <input class="input-info form-control" type="text" name="user_id" id="user_id" placeholder="1. 사용자ID" maxlength="15" readonly>
                        <input class="input-info form-control" type="email" name="user_email" id="user_email" placeholder="2. 이메일" maxlength="30" >
                        <input class="input-info form-control phoneNumber" type="tel" name="user_tel" id="user_tel" placeholder="3. 연락처" maxlength="15">
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btnUserInfoSave">수정</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- C: modal -->

<%@ include file="../inc/adminFooter.jsp" %>

</body>
</html>


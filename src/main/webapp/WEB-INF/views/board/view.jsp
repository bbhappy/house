<%@ page contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<%@ include file="../inc/adminHeader.jsp" %>
<script type="text/javascript" src="<c:url value="/resources/js/board/view.js" />" ></script>

<%
    String referer = request.getHeader("REFERER");
    System.out.println(referer);
    if(referer == null){
%>
<script>
    alert("URL을 직접 입력해서 접근하셨습니다.\n정상적인 경로를 통해 다시 접근해 주세요.");
    document.location.href="/";
</script>
<%
        return;
    }
%>

<section>
    <div class="col-xl-10 col-lg-9 col-md-8 ml-auto bg-light fixed-top py-2 top-navbar">
        <div class="row align-items-center">
            <div class="col-md-4">
                <h4 class="text-secondary text-uppercase mb-2 mt-2">자유게시판 - 상세보기</h4>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-10 col-md-8 col-lg-9 ml-auto">
                <div class="row pt-md-5 mt-md-3 mb-5">
                    <div class="col-xl-12 col-md-12 col-sm-12 p2">
                        <input type="hidden" id="chk_board_no" value="${boardList.idx}" />
                        <input type="hidden" id="login_id" value="${sessionScope.user_id}" />

                        <table class="table table-bordered mt-1">
                            <colgroup>
                                <col style="width:15%;">
                                <col>
                                <col style="width:15%;">
                                <col>
                            </colgroup>
                            <thead>
                            <tr>
                                <th colspan="4">
                                    <div class="col-md-12">
                                        <span>${boardList.title}</span>
                                    </div>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th>작성자</th>
                                <td>${boardList.writer}</td>
                                <th>등록일</th>
                                <td>
                                    <fmt:parseDate value="${boardList.req_dt}" pattern="yyyy-MM-dd'T'HH:mm:ss" var="parsedDateTime" type="both" />
                                    <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${parsedDateTime}" />
                                </td>
                            </tr>
                            <tr>
                                <th>조회수</th>
                                <td><fmt:formatNumber value="${boardList.view}" /></td>
                                <td colspan="2">
                                    <c:if test="${boardList.writer_id eq sessionScope.user_id }" >
                                        <button class="btn btn-info btn-md" onclick="boardModify()">수정</button>
                                        <button class="btn btn-danger btn-md" onclick="boardDelete()">삭제</button>
                                    </c:if>
                                    <button class="btn btn-primary btn-md" onclick="history.back(-1)">목록으로</button>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div class="col-md-9">
                                        <div class="board-contents" style="min-height: 430px;">
                                            ${boardList.content}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <div class="card card-body">
                            <c:if test="${not empty sessionScope.user_id}"> <!-- 댓글 작성 -->
                                <div class="row reply_write mb-5">
                                    <div class="col-2">
                                            ${sessionScope.user_id}
                                    </div>
                                    <div class="col-8" class="input_reply_div">
                                        <input class="w-150 form-control" id="input_reply0" type="text" placeholder="댓글입력..." maxlength="1000">
                                    </div>
                                    <div class="col-2">
                                        <button type="button" class="btn btn-success btn-md write_reply" onclick="WriteReReply(0)">등록</button>
                                    </div>
                                </div>
                            </c:if>
                            <div class="reply-list reply-list${boardList.idx}"> <!-- 댓글이 목록이 들어가는 곳 -->

                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<%@ include file="../inc/adminFooter.jsp" %>

</body>
</html>
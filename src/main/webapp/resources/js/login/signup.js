
let submitFlag = false;

function keyClear(){
    $("#check_num").val("");
    $("#compare_num").val("");
}

function formCheck() {

    if(submitFlag){ return false; }
    
    //검증
    if( ($("#user_id").val()).length <3) {
        setMessage('아이디는 3자리 이상이어야 합니다.',  document.getElementById("user_id") );
        return false;
    }

    if(! /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,12}$/.test($("#user_pw").val()) ){
        setMessage('영어+숫자+특수문자(!@$%^&*)<br>5~8자리를 사용해야 합니다', document.getElementById("user_pw") );
        return false;
    }

    if(/(\w)\1\1\1/.test( $("#user_pw").val() )){
        setMessage('같은 문자를 4번 이상 사용금지', document.getElementById("user_pw") )
        return false;
    }

    if( $("#user_pw").val().search( $("#user_id").val() ) > -1){
        setMessage('비밀번호에 아이디가 포함됨', document.getElementById("user_pw") )
        return false;
    }

    if( $("#user_nm").val() == "") {
        setMessage('이름을 입력하세요.', document.getElementById("user_nm") );
        return false;
    }

    if( $("#user_tel").val() == "") {
        setMessage('연락처를 입력하세요.', document.getElementById("user_tel") );
        return false;
    }

    if( $("#user_email").val() == "") {
        setMessage('이메일을 입력하세요.', document.getElementById("user_email") );
        return false;
    }

    if( $("#check_num").val() == "" ){
        setMessage('인증번호를 입력하세요.', document.getElementById("check_num") );
        return false;
    }

    if( $("#compare_num").val() == "" ){
        setMessage('인증메일이 전송되지 않았습니다.', document.getElementById("user_email") );
        return false;
    }

    if( $("#check_num").val() != $("#compare_num").val() ){
        setMessage('올바른 인증번호를 입력하세요.', document.getElementById("check_num") );
        return false;
    }

    //form
    submitFlag = true;
    var formData = $("#userInfoFrm").serialize();

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false,
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        url: "/login/userInfo",
        data: formData,
        dataType: "json",
        async : false,
        success: function (result) {
            if( result.status == "ok"){
                $("#compare_num").val("");
                showModal("alertModal" ,"사용자 등록 완료.");
                sleep2(2500).then(() => location.href="/" );
            }else if(result.status == "duplicatedId"){
                setMessage('사용중인 id',  document.getElementById("user_id") );
            }else if(result.status == "duplicatedMail"){
                setMessage('사용중인 email',  document.getElementById("user_email") );
            }else{
                showModal("alertModal" ,result.status);
            }
        },
        error: function () {
            showModal("alertModal" ,"페이지에 에러가 있습니다!");
        }
    });
    setTimeout(function (){submitFlag = false;}, 3000);
}

function mailCheck(){

    if(submitFlag){ return false; }

    if( $("#user_email").val() == "") {
        setMessage('이메일을 입력하세요.', document.getElementById("user_email") );
        return false;
    }

    $("#check_num").val("");
    $("#compare_num").val("");
    submitFlag = true;

    $.ajax({
        type: "GET",
        enctype: 'multipart/form-data',
        processData: false,
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        url: "/login/checkEmail?email=" + $("#user_email").val(),
        dataType: "text",
        async : false,
        success: function (result) {
            if (result == 'duplicatedMail') {
                showModal("alertModal", "이미 등록된 메일이 있습니다.");
            }else if(result != null && result != ""){
                $("#compare_num").val(result);
                $("#check_num").focus();
                showModal("alertModal", "인증 메일이 전송 되었습니다.");
            }else{
                showModal("alertModal" ,"이메일이 전송되지 않았습니다.");
            }
        },
        error: function () {
            showModal("alertModal" ,"페이지에 에러가 있습니다!");
        }
    });
    setTimeout(function (){submitFlag = false;}, 3000);
}

$(document).on("keyup", ".phoneNumber", function() {
    $(this).val( $(this).val().replace(/[^0-9]/g, "").replace(/(^02|^0505|^1[0-9]{3}|^0[0-9]{2})([0-9]+)?([0-9]{4})$/,"$1-$2-$3").replace("--", "-") );
});

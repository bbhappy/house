
let submitFlag = false;

function formCheck() {

    if(submitFlag){ return false; }

    var inputId 	= $('#user_id').val();
    var inputPass 	= $('#user_pw').val();

    if (inputId ==""){$('#inputNoDiv').addClass('d-block show').removeClass('d-none hide');return;}
    else $('#inputNoDiv').removeClass('d-block show').addClass('d-none hide');

    if (inputPass ==""){$('#inputPassDiv').addClass('d-block show').removeClass('d-none hide');return;}
    else $('#inputPassDiv').removeClass('d-block show').addClass('d-none hide');

    //form
    submitFlag = true;
    var formData = $("#loginForm").serialize();

    $.ajax({
        type: "POST",
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        url: "/login/start",
        data: formData,
        dataType: "json",
        async : false,
        success: function (result) {
            switch(result.status){
                case "success" : location.href="dashboard"; break;
                case "pw_check" :
                    showModal("alertModal" ,"비밀번호를 확인 하세요.\n\n5/" + result.trial + " 회 실패.");
                    break;
                case "trial_fail" :
                    showModal("alertModal" ,"시도횟수를 초과 하였습니다. 비밀번호 찾기를 이용하세요.");
                    break;
                case "no_user" :
                    showModal("alertModal" ,"존재하지 않는 사용자 입니다.");
                    break;
                default :
                    showModal("alertModal",result.status);
                    break;
            }
        },
        error: function () {
            showModal("alertModal" ,"페이지에 에러가 있습니다!");
        }
    });

    setTimeout(function (){submitFlag = false;}, 3000);
}


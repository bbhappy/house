
let submitFlag = false;

function pwChange(){

    if(submitFlag){ return false; }

    if( $("#user_id").val() == "" ){
        showModal("alertModal" , "아이디가 올바르지 않습니다.<br> 올바른 경로가 아닙니다.");
        return false;
    }

    if( $("#user_email").val() == "" ){
        showModal("alertModal" , "이메일이 올바르지 않습니다.<br> 올바른 경로가 아닙니다.");
        return false;
    }

    if( $("#user_pw").val() == "" ){
        showModal("alertModal" , "변경할 비밀번호를 입력하세요.");
        $("#user_pw").focus();
        return false;
    }

    if(! /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,12}$/.test($("#user_pw").val()) ){
        showModal("alertModal" , "영어+숫자+특수문자(!@$%^&*)<br>5~8자리를 사용해야 합니다");
        $("#user_pw").focus();
        return false;
    }

    if(/(\w)\1\1\1/.test( $("#user_pw").val() )){
        showModal("alertModal" , "비밀번호에 같은 문자를 4번 이상 사용금지");
        $("#user_pw").focus();
        return false;
    }

    if( $("#user_pw").val().search( $("#user_id").val() ) > -1){
        showModal("alertModal" , "비밀번호에 아이디가 포함됨");
        $("#user_pw").focus();
        return false;
    }

    if( $("#check_pw").val() == "" ){
        showModal("alertModal" , "비밀번호를 다시 입력해 주세요.");
        $("#check_pw").focus();
        return false;
    }

    if( $("#user_pw").val() != $("#check_pw").val() ){
        showModal("alertModal" , "입력된 비밀번호가 다릅니다.");
        $("#check_pw").focus();
        return false;
    }

    submitFlag = true;
    var formData = $("#frm").serialize();

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false,
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        url: "/login/changePw",
        data: formData,
        dataType: "text",
        async : false,
        success: function (result) {
            if(result == "ok"){
                showModal("alertModal", "비밀번호 변경 성공");
                setTimeout( location.href="/" , 3000);

            }else if( result =="fail"){
                showModal("alertModal", "비밀번호 변경 실패");
                return false;

            }else if( result =="no_exist"){
                showModal("alertModal", "등록된 사용자가 아닙니다.");
                setTimeout( location.href="/" , 3000);
                location.href="/";
            }
        }
    });
}

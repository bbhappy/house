let submitFlag = false;

// 서머노트 파일 업로드
function uploadSummernoteImageFile(file, el) {

    let data = new FormData();
    data.append("file", file);
    $.ajax({
        data : data,
        type : "POST",
        url : "/board/uploadSummernoteImageFile",
        contentType : false,
        enctype : 'multipart/form-data',
        processData : false,
        success : function(data) {
            if(data.responseCode == "success") {
                setTimeout(function () {
                    $(el).summernote('insertImage', data.url, function ($image) {
                        $image.css('width', "20%");
                    });
                }, 2000);
            }else if(data.responseCode == "extension"){
                showModal("alertModal","gif,jpg,png만 가능합니다.");
                return false;
            }else{
                showModal("alertModal","파일 업로드에 실패 하였습니다.");
                return false;
            }
        }
    });
}

//글자수 체크 //태그와 줄바꿈, 공백을 제거하고 텍스트 글자수만 가져옵니다.
function setContentsLength(str, index) {
    var textCnt = 0;                                    //총 글자수
    var maxCnt = 1000;                                  //최대 글자수
    var editorText = f_SkipTags_html(str);              //에디터에서 태그를 삭제하고 내용만 가져오기
    //editorText = editorText.replace(/\s/gi,"");         //줄바꿈 제거
    //editorText = editorText.replace(/&nbsp;/gi, "");    //공백제거
    textCnt = editorText.length;
    if(maxCnt > 0) {
        if(textCnt > maxCnt) {
            $('#summernote').summernote('code', str.slice(0 , -1 ));
            return false;
        }else{
            $("#test_cnt").html( "( "+ textCnt + " / " + maxCnt + " )");
        }
    }
}

//에디터 내용 텍스트 제거
function f_SkipTags_html(input, allowed) { // 허용할 태그는 다음과 같이 소문자로 넘겨받습니다. (<a><b><c>)
    allowed = (((allowed || "") + "").toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join('');
    var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
    commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
    return input.replace(commentsAndPhpTags, '').replace(tags, function ($0, $1) {
        return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
    });
}

// 글저장하기
function boardWrite(){

    if(submitFlag){ return false; }

    if($("#title").val() == ""){
        showModal("alertModal","제목을 입력하세요");
        return false;
    }

    if ($('#summernote').summernote('isEmpty')) {
        showModal("alertModal","내용을 입력하세요");
        return false;
    }

    submitFlag = true;
    var formData = $("#articleForm").serialize();
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        url: "/board/insertBoard",
        data: formData,
        dataType: "text",
        async : false,
        success : function(data) {
            if(data == "success") {
                showModal("alertModal","저장 되었습니다.");
                sleep2(2500).then(() => location.href="/board/listPage" );
                return false;
            }else{
                showModal("alertModal","저장에 실패 하였습니다.");
                setTimeout(function (){submitFlag = false;}, 3000);
                return false;
            }
        }
    });
}




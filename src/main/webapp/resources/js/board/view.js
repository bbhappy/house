/** 게시판->자유게시판 : 읽기 */

let submitFlag = false;

$(document).ready(function (){
    replyList($("#chk_board_no").val());
})

function open_fun( no ){
    document.getElementById("link"+ no ).innerHTML = "<a href='#' data-toggle='collapse' data-target='#re_reply"+ no +"' aria-expanded='false' aria-controls='collapseExample' onclick=\"close_fun("+ no +")\">댓글▲</a>";
}

function  close_fun( no ){
    document.getElementById("link"+ no ).innerHTML = "<a href='#' data-toggle='collapse' data-target='#re_reply"+ no +"' aria-expanded='false' aria-controls='collapseExample' onclick=\"open_fun("+ no +")\">댓글▼</a>";
}

function replyList(chk, section = 0){
    $.ajax({
        type: "GET",
        enctype: 'multipart/form-data',
        processData: false,
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        url: "/board/replyList?chk_board_no=" + chk,
        dataType: "JSON",
        async : false,
        success : function(result) {

            // 댓글 목록을 html로 담기
            let listHtml = "";

            let data = result.boardReplyList;
            for(const i in data){
                let no = data[i].no;
                let bno = data[i].bno;
                let grp = data[i].grp;
                let grps = data[i].grps;
                let grpl = data[i].grpl;
                let writer = data[i].writer;
                let content = data[i].content;
                let wdate = data[i].wdate;

                //console.log(grpl);   // 모댓글일땐 0, 답글일땐 1

                if(content == ""){
                    listHtml += "       <table class='col-12'>";
                    listHtml += "           <tr>";
                    listHtml += "                <td class='col-1' style='padding:0px;'></td>";
                    listHtml += "               <td colspan='11'> (삭제된 댓글입니다) </td>";
                    listHtml += "           </tr>";
                    listHtml += "       </table>";
                }else{
                    if(grpl == 0){ // 모댓글일때

                        listHtml += "<div class='rereply-content'>";
                        listHtml += "       <table class='col-12'>";
                        listHtml += "           <tr>";
                        listHtml += "                <td class='col-1' style='padding:10px;'>";
                        listHtml += "                 <a href='#'> ";
                        listHtml += "                    <img style='width:40px;height:40px;' src='/resources/images/none_profile.png'/>";
                        listHtml += "                 </a> ";
                        listHtml += "               </td>";
                        listHtml += "               <td class='col-2' style='padding:0px;'>";
                        listHtml += "                <span>";
                        listHtml += "               <b>"+ writer.substring(0,2)+ "*****"  +"</b>";
                        listHtml += "                 <BR><span id='link"+ no +"'><a href='#' data-toggle='collapse' data-target='#re_reply"+ no +"' aria-expanded='false' aria-controls='collapseExample' onclick=\"open_fun("+ no +")\">댓글▼</a></span>";
                        listHtml += "                </span>";
                        listHtml += "               </td>";
                        listHtml += "               <td>";
                        listHtml += "                <span class='col-7 ml-2'>";
                        listHtml +=                 content;
                        listHtml += "                </span>";
                        listHtml += "               </td>";
                        listHtml += "               <td class='col-2'>";
                        listHtml +=                  wdate;

                        //현재 사용자가 이 댓글의 작성자일때 삭제 버튼이 나온다.
                        if($("#login_id").val() == writer){
                            listHtml += "             <a href='javascript:;' onclick=\"deleteReply("+ grp +","+ no +")\">삭제</a>";
                        }

                        listHtml += "               </td>";
                        listHtml += "           </tr>";
                        listHtml += "       </table>";
                        listHtml += "</div>";

                    }else{ // 답글일때

                        listHtml += "       <table class='col-12'>";
                        listHtml += "           <tr>";
                        listHtml += "                <td class='col-1' style='padding:0px;'></td>";
                        listHtml += "                <td class='col-1' style='padding:10px;'>";

                        if($("#login_id").val() == writer){
                            listHtml += "                 <a href='javascript:;'><img style='width:40px;height:40px;' src='/resources/images/none_profile3.png'/></a> ";
                        }else{
                            listHtml += "                 <a href='javascript:;'><img style='width:40px;height:40px;' src='/resources/images/none_profile2.png'/></a> ";
                        }

                        listHtml += "               </td>";
                        listHtml += "               <td class='col-2' style='padding:0px;'>";
                        listHtml += "                <span>";
                        listHtml += "               <b>"+ writer.substring(0,2)+ "*****"  +"</b>";
                        listHtml += "                </span>";
                        listHtml += "               </td>";
                        listHtml += "               <td>";
                        listHtml += "                <span class='col-6 ml-2'>";
                        listHtml +=                 content;
                        listHtml += "                </span>";
                        listHtml += "               </td>";
                        listHtml += "               <td class='col-2'>";
                        listHtml +=                  wdate;

                        //현재 사용자가 이 댓글의 작성자일때 삭제 버튼이 나온다.
                        if($("#login_id").val() == writer){
                            listHtml += "             <a href='javascript:;' onclick=\"deleteReply("+ grp +","+ no +")\">삭제</a>";
                        }

                        listHtml += "               </td>";
                        listHtml += "           </tr>";
                        listHtml += "       </table>";
                    }
                }

                // ================================
                if(grpl == 0){// 모댓글 의 하위 댓글 div 구역의 생성
                    listHtml += "  <div class='collapse row rereply_write' id='re_reply"+ no +"'>";
                    listHtml += "   <hr class='col-11' style='border: solid 1px lightblue;'>";
                }

                let k = parseInt(i);
                if(  k < data.length-1 && grp != data[k+1].grp ){
                    listHtml += "       <table class='col-12'>";
                    listHtml += "           <tr>";
                    listHtml += "                <td class='col-1' style='padding:10px;'></td>";
                    listHtml += "                <td class='col-2' style='padding:10px;'>"+ $("#login_id").val() +"</td>";
                    listHtml += "                <td class='col-6'><input class='w-150 form-control' id='input_reply"+ grp +"' type='text' placeholder='댓글입력...' maxLength='1000'></td>";
                    listHtml += "                <td class='col-3'><button type='button' class='btn btn-success btn-md' onclick=\"WriteReReply("+ grp +")\" >등록 </button></td>";
                    listHtml += "           </tr>";
                    listHtml += "       </table>";
                    listHtml += "   <hr class='col-11' style='border: solid 1px lightblue;'>";

                    listHtml += "     </div>";
                }else if(i == data.length-1){
                    listHtml += "       <table class='col-12'>";
                    listHtml += "           <tr>";
                    listHtml += "                <td class='col-1' style='padding:10px;'></td>";
                    listHtml += "                <td class='col-2' style='padding:10px;'>"+ $("#login_id").val() +"</td>";
                    listHtml += "                <td class='col-6'><input class='w-150 form-control' id='input_reply"+ grp +"' type='text' placeholder='댓글입력...' maxLength='1000'></td>";
                    listHtml += "                <td class='col-3'><button type='button' class='btn btn-success btn-md' onclick=\"WriteReReply("+ grp +")\" >등록 </button></td>";
                    listHtml += "           </tr>";
                    listHtml += "       </table>";
                    listHtml += "   <hr class='col-11' style='border: solid 1px lightblue;'>";

                    listHtml += "     </div>";
                }

            };

            // 댓글 리스트 부분에 받아온 댓글 리스트를 넣기
            $(".reply-list"+ chk).html(listHtml);

            // 댓글에 댓글인 경우 댓글 불러오고 해당 구역 open
            if (section > 0) $("#re_reply"+ section).addClass("show");

        }, error : function() {
            alert('서버 에러');
        }
    });
}

// 답글 달기 버튼 클릭
const WriteReReply = function(grp) {

    var content = $.trim($("#input_reply" + grp).val());

    if(content == ""){
        showModal("alertModal","글을 입력하세요!");
    }else{

        let mode = "parent";
        if(grp > 0){ mode = "child"}

        $.ajax({
            url : "/board/replyInsert",
            type : 'GET',
            data :  {
                "mode" : mode,
                "bno" : $("#chk_board_no").val(),
                "grp" : grp,
                "content" : content
            },
            dataType : "text",
            success : function(result) {
                if (result == "error"){
                    showModal("alertModal","ERROR!");
                    return false;
                }else{
                    $("#input_reply" + grp).val("");
                    replyList($("#chk_board_no").val() , grp);
                    return false;
                }
            },
            error : function() {
                showModal("alertModal","서버 에러!");
            }
        });

    };
};


// 댓글 삭제
const deleteReply = function(grp, no){

    $.ajax({
        url : '/board/replyDelete',
        type : 'GET',
        data : {
            no : no,
            grp : grp
        },
        dataType : "text",
        success : function(result) {
            if (result == "error"){
                showModal("alertModal","ERROR!");
                return false;
            }else{
                replyList($("#chk_board_no").val() , grp);
                return false;
            }
        },
        error : function() {
            alert('서버 에러');
        }
    });
};





function boardModify(){ // 글 수정

    if(submitFlag){ return false; }
    submitFlag = true;
    let chk_board_no = $("#chk_board_no").val();

    location.href="/board/modifyPage?chk_board_no=" + chk_board_no;
    setTimeout(function (){submitFlag = false;}, 3000);
}

function boardDelete(){ // 글 삭제

    if(submitFlag){ return false; }
    submitFlag = true;

    $.ajax({
        type: "GET",
        enctype: 'multipart/form-data',
        processData: false,
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        url: "/board/boardDelete?chk_board_no=" + $("#chk_board_no").val(),
        dataType: "text",
        async : false,
        success: function (result) {
            showModal("alertModal" , "삭제되었습니다.");
            sleep2(2500).then(() => location.href="/board/listPage" );
            setTimeout(function (){submitFlag = false;}, 3000);
        },
        error: function () {
            showModal("alertModal" ,"페이지에 에러가 있습니다!");
            setTimeout(function (){submitFlag = false;}, 3000);
        }
    });
}
/** 게시판->자유게시판 */

let submitFlag = false;

function boardWrite(){
    if(submitFlag){ return false; }
    submitFlag = true;

    location.href="/board/writePage";
    setTimeout(function (){submitFlag = false;}, 3000);
}

/** 검색버튼 click */
function searchDetail(){

    if(submitFlag){ return false; }
    submitFlag = true;

    var theForm 		= document.requestForm;
    theForm.method 		= "get";
    theForm.action 		= "./listPage";
    theForm.submit();
    setTimeout(function (){submitFlag = false;}, 3000);
};

$(document).ready(function(){

    $(".rowTr").click(function(){
        if(submitFlag){ return false; }
        submitFlag = true;

        const chk_board_no = $(this).closest("tr").find("#chk_board_no").val();
        if(chk_board_no == null) return false;
        location.href = "/board/view?chk_board_no=" + chk_board_no;
        setTimeout(function (){submitFlag = false;}, 3000);
    });
});
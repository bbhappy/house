/* Copyright (c) 2011 Smileweb co., Ltd.  All rights reserved.  www.smileweb.co.kr */

function isNull( text ){
	 if( text == null ) return true;
	 var result = text.replace(/(^\s*)|(\s*$)/g, "");
	 if( result ) return false; else return true;
}
function CheckNumberMinLimit (fl, min) { t = fl.value ; t = parseInt(t,10);
	if ( t < min ) {alert( min + " 이상 입력해주세요.") ;
		try{ fl.value=""; fl.focus() ;} catch (e){}		
		return false; }
}
function CheckNumber(fl) {	t = fl.value ; 
	for(i=0;i<t.length;i++){if (t.charAt(i)<'0' || t.charAt(i)>'9'){ alert("숫자만 입력해주세요."); fl.value=""; fl.focus();return false;}}
}
function CheckFloat(fl) {t=fl.value;
	for(i=0;i<t.length;i++){if (  t.charAt(i)!='.' && (t.charAt(i)<'0' || t.charAt(i)>'9') ){alert(" 소수점과 숫자만 입력해주세요."); fl.value=""; fl.focus();return false;}}
}
function CheckMinus(fl){t=fl.value;
	for(i=0;i<t.length;i++){if (  t.charAt(i)!='-' && (t.charAt(i)<'0' || t.charAt(i)>'9') ){alert(" '-'와 숫자만 입력해주세요."); fl.value="";fl.focus() ;return false;}}
}
function CheckBar(fl) {t=fl.value ;
	for(i=0;i<t.length;i++){if(t.charAt(i)!='|' && (t.charAt(i)<'0' || t.charAt(i)>'9') ){alert(" '|'와 숫자만 입력해주세요.");fl.value=""; fl.focus();return false;}}
}

var alpha ='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; var alphaS='abcdefghijklmnopqrstuvwxyz';
var numeric ='1234567890'; 
var dash ='_-'; var special ="`%^&{}|=<>?#'$\\\";:";
var dot ='.'; var email ='@'
var selftextgubun =',';
function checknormNeo(target, cmt, astr, lmax,lshort) { var i; var t =trim(target.val());
	if (t.length == 0){ alert(cmt + '(을)를 정확히 기재해 주십시오.'); target.focus(); return true;}
	if (lshort != 0 && t.length < lshort){alert(cmt + '는 ' + lshort + '자 이상만 가능합니다.');target.focus(); return true;}
	if (lmax != 0 && t.length > lmax){alert(cmt + '는 ' + lmax + '자 이내만 가능합니다.');target.focus();return true;}
	if (astr.length >= 1) {
		for (i = 0; i < t.length; i++)
		if (astr.indexOf(t.substring(i, i+1)) < 0) {alert(cmt + '에 정확히 입력해 주십시오.');
			try {target.focus();}catch (ex) {}
			return true;
		}
	}
	return false
}
function checknorm(target, cmt, astr, lmax,lshort) { var i; var t =trim(target.value);
	if (t.length == 0){ alert(cmt + '(을)를 정확히 기재해 주십시오.'); target.focus(); return true;}
	if (lshort != 0 && t.length < lshort){alert(cmt + '는 ' + lshort + '자 이상만 가능합니다.');target.focus(); return true;}
	if (lmax != 0 && t.length > lmax){alert(cmt + '는 ' + lmax + '자 이내만 가능합니다.');target.focus();return true;}
	if (astr.length >= 1) {
		for (i = 0; i < t.length; i++)
		if (astr.indexOf(t.substring(i, i+1)) < 0) {alert(cmt + '에 정확히 입력해 주십시오.');
			try {target.focus();}catch (ex) {}
			return true;
		}
	}
	return false
}

function ltrim(str){var strreturn="";
    if(jQuery.type(str)=='undefined'){console.log("base.js error:ltrim"); return "";} /* ywlee@191128 */
	for(i=0;i<=str.length-1;i++){
		if (str.charAt(i) !=' '){strreturn=str.substring(i,str.length); return strreturn; }
	}; return strreturn;
}
function rtrim(str){var strreturn = "";
    if(jQuery.type(str)=='undefined'){console.log("base.js error:rtrim"); return "";} /* ywlee@191128 */
	for(i=str.length-1; i >=0  ; i--){
		if (str.charAt(i) != ' '){ strreturn = str.substring(0,i+1); return strreturn; }
	}; return strreturn;
}
function trim(str){	return rtrim(ltrim(str)); }
function checknot(target,cmt,astr){ var i; var t = target.value;
	if (astr.length == 0)	astr = special;
	for (i=0; i<t.length; i++)	{
		if(astr.indexOf(t.substring(i,i+1)) >= 0){ alert(cmt + "에 [~()_-*@!'+/]외의 특수문자는 사용불가 합니다. "); target.focus(); return true;}
	}; return false;
}
function checknot2(target,cmt,special){
	var i; var t=target.value;
	if (special.length==0)	return false;
	for (i=0; i<t.length; i++){
		if(special.indexOf(t.substring(i,i+1)) >= 0){alert(cmt + "에 특수문자 ["+ special + "]  사용불가 합니다. ");target.focus(); return true;}
	}; return false;
}
function pop_closed(){try{window.close();}catch(e){}}
function pop_reload_close(){try{opener.location.reload();}catch(e){};	pop_closed();}
function pop_move_close(url){try {opener.location.href=url;}catch(e){};pop_closed();}
function iframe_reload_close(){try{parent.location.reload();}catch(e){};pop_closed();}
String.prototype.replaceAll = function(strTarget, strSubString ){
	var strText = this;	var intIndexOfMatch = strText.indexOf( strTarget );
	while (intIndexOfMatch != -1){strText=strText.replace(strTarget, strSubString);intIndexOfMatch=strText.indexOf( strTarget );};
	return( strText );}
String.prototype.contains = function(target){ 
	str=this.toUpperCase(); target=target.toUpperCase();
	var strLength=str.length; var targetLength=target.length;
	if( str==target){ return true; }
	if( strLength < targetLength ){ return false; }
	for (var i=0; i<strLength; i++) {
		if (str.substr(i, targetLength) == target){ return true;}		
	}; return false;	
} 
Array.prototype.contains = function(element) { for (var i = 0; i < this.length; i++) {if (this[i]==element) { return true;}};return false;} 

function number_format(val){
	if(jQuery.type(val)=='undefined'){ return val; }
	if(val==''){ return val; }
	val=parseFloat(val); ret_val=val.toLocaleString();
	return(ret_val);
}
function renumber_format(val){
	if(jQuery.type(val)=='undefined'){ return val; }
	if(val==''){ return val; }
	var returnValue = val.replace(",","");
  returnValue = returnValue.replace(",","");
	returnValue = returnValue.replace(",","");
	return returnValue;
}

function getRequestParam(key){
	if(window.location.href.indexOf('?') > 0){
		var params =  window.location.href.slice(window.location.href.indexOf('?') + 1).split('#');
		var requstParams = decodeURIComponent(params[0]).split('&');
		for(var i = 0; i < requstParams.length; i++){
			var tempArray = requstParams[i].split('=');
			if(tempArray[0] == key){return tempArray[1];}
		}
	};return '';
}
//change parameter
function getChangeParam(data){var paramData = {};
	if(window.location.href.indexOf('?') > 0){
		var params =  window.location.href.slice(window.location.href.indexOf('?') + 1).split('#');
		var requstParams = decodeURIComponent(params[0]).split('&');
		for(var i = 0; i < requstParams.length; i++){
			var tempArray = requstParams[i].split('='); paramData[trim(tempArray[0])] = trim(tempArray[1]);
		}
	}; $.extend(paramData, data); return paramData;
}
//parameter return
function getParamData(){
	var paramData = {};
	if(window.location.href.indexOf('?') > 0)	{
		var params =  window.location.href.slice(window.location.href.indexOf('?') + 1).split('#');
		var requstParams = decodeURIComponent(params[0]).split('&');
		for(var i = 0; i < requstParams.length; i++){
			var tempArray = requstParams[i].split('=');
			paramData[trim(tempArray[0])] = trim(tempArray[1]);
		}
	};return paramData;
} 
function getParamStrToData(str){
	var paramData = {};
	var requstParams = decodeURIComponent(str).split('&');
	for(var i = 0; i < requstParams.length; i++){
		var tempArray = requstParams[i].split('=');
		paramData[trim(tempArray[0])] = trim(tempArray[1]);
	};return paramData;
}
function getParamUrl(paramData){
	var returnParam = '';var paramCnt = 0;
	for(key in paramData){
		if(paramCnt > 0) returnParam += '&';
		returnParam += key+'='+encodeURIComponent(paramData[key]);
		paramCnt++;
	};return  returnParam;
}
function ajax_error(){ /*alert("error in ajax."); */ }
function JqueryAjax(url , type , data, Result){  if(type==null) type='POST'; jQuery.ajax({ url:url, type: type, data: data, success: Result, error: ajax_error 	});}
function AjaxXhrRequest(url,data,Result,type){ if(type==null) type='POST';  jQuery.ajax({url:url, type: type, data: data, success: Result, error: ajax_error });}
function AjaxXhrRequest2(url,data,Result,type,before,end,etc){ if(type==null) type='POST';  jQuery.ajax({url:url, type: type, data: data, success: Result, error: ajax_error,beforeSend:before,complete:end });}
function stripJS(str){if(str==null)return str; if(str=='')return str;	
	str=str.replaceAll("&#36;", "$").replaceAll("<br/>", "\n").replaceAll("<br>", "\n").replaceAll("\\\"", "\"").replace(/\'/g, "'"); str=str.replaceAll("&amp;lt;", "<").replaceAll("&amp;gt;", ">").replaceAll("&quot;", "\"").replaceAll("&lt;", "<").replaceAll("&gt;", ">");
	return str;}

function AjaxPostJSON(Url,Data,Result,Type,Before,End,Error){
	if(Error==null)Error=ajax_error; 
	$.ajax({
		type:"POST",
		processData: false,
    contentType: false,	
		url:Url,data:Data,success:Result,error:Error,beforeSend:Before,complete:End});
}

function AjaxGetJSON(Url,Data,Result,Type,Before,End,Error){
	if(Error==null)Error=ajax_error; 
	$.ajax({dataType:"json",url:Url,data:Data,success:Result,error:Error,beforeSend:Before,complete:End});
}
function AjaxGetJSONP(callbackFunc,Url,Data,Result,Type,Before,End,Error){
	if(Error==null)Error=ajax_error; 
	jQuery.ajax({dataType:"json",url:Url,data:Data,callback:callbackFunc,success:Result,error:Error,beforeSend:Before,complete:End});
} 
function getNodeValue (nodes){var nodeValue = "";for (i=0; i<nodes.length; i++){try { nodeValue = nodes[i].childNodes[0].nodeValue;} catch(e) {nodeValue = "찾을수 없습니다.";}} return nodeValue;}
function getNodeAt(nodes, aname){var nodeValue = "";
	for (i=0; i < nodes.length; i++){ for ( j = 0; j < nodes[i].attributes.length ; j++ ){nodeValue = nodes[i].attributes[j]; if (nodeValue.name == aname) break;}	}
	return nodeValue.value;}
function ChildNodesDel(elementId) {
	var obj = document.getElementById(elementId);
	if (obj == null) return;
	for(var i = obj.childNodes.length - 1; i >= 0; i--) {obj.removeChild(obj.childNodes.item(i));}}
function sleep(msecs){ var start=new Date().getTime();var cur=start;
	while(cur - start < msecs){cur=new Date().getTime();}}

//3자리 단위마다 콤마 생성
function addCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
 
//모든 콤마 제거
function removeCommas(x) {
    if(!x || x.length == 0) return "";
    else return x.split(",").join("");
}

//숫자 한글변환
function numberToKorean(num) {
	num = num +"";
  var hanA = new Array("","일","이","삼","사","오","육","칠","팔","구","십");
  var danA = new Array("","십","백","천","","십","백","천","","십","백","천","","십","백","천");
  var result = "";  
	for(i=0; i<num.length; i++) {
		str = "";
		han = hanA[num.charAt(num.length-(i+1))];
		if(han != "")
			str += han+danA[i];
		if(i== 4) str += "만";
		if(i== 8) str += "억";
		if(i== 12) str += "조";
		result = str + result;
	}
	if(num != 0)
		result = result + "원";
  return result ;
} 

// 공통 모달창 닫기
function hideModal() {
  $('#alertModal').modal('hide');
}

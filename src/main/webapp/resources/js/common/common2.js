
function showModal(id,msg){
    $("#alertModal-body").html("<p>"+ msg +"</p>");
    $("#"+ id).modal('show');
}

function hideModal(id){
    $("#alertModal-body").html("");
    $("#"+ id).modal('hide');
}

function sleep2(ms) {
    return new Promise((r) => setTimeout(r, ms));
}

function setMessage(msg, element){
    document.getElementById("msg").innerHTML = `<i class="fa fa-exclamation-circle"> ${msg}</i>`;
    if(element) {
        element.select();
    }
}

function userAuthSave(){
    submitFlag = true;
    var formData = $("#requestForm").serialize();

    $.ajax({
        type: "POST",
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        url: "/basic/auth_save",
        data: formData,
        dataType: "text",
        async : false,
        success: function (result) {
            if(result == "success") location.reload();
        },
        error: function () {
            showModal("alertModal" ,"페이지에 에러가 있습니다!");
        }
    });

    setTimeout(function (){submitFlag = false;}, 3000);
}
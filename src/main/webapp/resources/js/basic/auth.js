 /** 환경설정->권한관리 */

 let submitFlag = false;

$(document).ready(function(){

    /** 리스트 전체선택 checkbox */
    $("#allCheck").click(function(){
        //만약 전체 선택 체크박스가 체크된상태일경우
        if($("#allCheck").prop("checked")) {
            $("input[type=checkbox]").prop("checked",true);
        } else {
            $("input[type=checkbox]").prop("checked",false);
        }
    });


    /** 권한 설정 페이지 이동 */
    $('.btnGrant').on('click', function() {
        if(submitFlag){ return false; }
        submitFlag = true;
        const it_user_id   = $(this).closest("tr").find("#chk_user_id").val();
        const it_user_nm   = $(this).closest("tr").find("#chk_user_nm").val();
        location.href = "/basic/auth_modify?it_user_id=" + it_user_id +"&it_user_nm=" + it_user_nm;
        setTimeout(function (){submitFlag = false;}, 3000);
    });


    /** 접속실패 횟수 초기화 */
    $('.btnPwClear').on('click', function(event) {
        if(submitFlag){ return false; }
        submitFlag = true;

        var check = confirm("접속 실패 횟수를 초기화 하시겠습니까?");
        if(check) {
            const it_user_id = $(this).closest("tr").find("#chk_user_id").val();
            $.ajax({
                type: "GET",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                url: "/basic/login_fail_clear?it_user_id=" + it_user_id,
                dataType: "text",
                async : false,
                success: function (result) {
                    if(result=="success"){
                        showModal("alertModal" ,"처리 되었습니다.");
                        setTimeout(function (){ location.href= "/basic/auth";  }, 3000);
                    }else{
                        showModal("alertModal" ,result);
                    }
                },error: function () {
                    showModal("alertModal" ,"페이지에 에러가 있습니다!");
                }
            });
        }
        setTimeout(function (){submitFlag = false;}, 3000);
    });


    /** 사용자 정보 팝업 */
    $('.rowTr').on('click', function(event) {
        if(submitFlag){ return false; }
        submitFlag = true;

        $("#userModal").find('form')[0].reset();//폼초기화
        $('#userForm input[type=hidden]').val('');

        const it_user_id = $(this).closest("tr").find("#chk_user_id").val();
        if(it_user_id == null) return false;

        $.ajax({
            type: "GET",
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            url: "/basic/user_info?it_user_id="+ it_user_id,
            dataType: "json",
            async : false,
            success: function (result) {

                $("#user_id").val(result.user_id);
                $("#user_email").val(result.user_email);
                $("#user_tel").val(result.user_tel);
                $("#user_pw").val("");
                $("#userModal").modal("show");

            },error: function () {
                showModal("alertModal" ,"페이지에 에러가 있습니다!");
            }
        });
        setTimeout(function (){submitFlag = false;}, 3000);
    });


    /** 사용자 정보 수정하기 */
    $("#btnUserInfoSave").on("click",function(){
        if(submitFlag){ return false; }
        submitFlag = true;

        if( $("#user_id").val() == ""){ return false; }
        var formData = $("#userForm").serialize();
        $.ajax({
            type: "POST",
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            url: "/basic/user_info_modify",
            data: formData,
            dataType: "text",
            async : false,
            success: function (result) {
                $("#userModal").modal("hide");
                if(result == "ok"){
                    showModal("alertModal", "사용자 정보 수정 성공");
                    setTimeout( location.href="/basic/auth" , 3000);
                }else if( result =="fail"){
                    showModal("alertModal", "사용자 정보 수정 실패");
                    return false;
                }else{
                    showModal("alertModal", result);
                }
            }
        });
        setTimeout(function (){submitFlag = false;}, 3000);
    });

});


/** 검색버튼 click */
function searchDetail(){

    if(submitFlag){ return false; }
    submitFlag = true;

    var theForm 		= document.requestForm;
    theForm.method 		= "get";
    theForm.action 		= "./auth";
    theForm.submit();
    setTimeout(function (){submitFlag = false;}, 3000);
};

/** 사용자 탈퇴처리 */
function btnDeleteCheckBox(){
    if(submitFlag){ return false; }
    submitFlag = true;

    var employeeNo 	= []; //시퀀스
    $('#userTable tbody tr').each(function (i, row) {
        var chk_it_no = $(this).closest("tr").find("#chk_it_no");
        if(chk_it_no.is(":checked") == true) {
            employeeNo.push(chk_it_no.val());
        }
    });

    if(employeeNo.length<1){  showModal("alertModal" ,"탈퇴할 사용자를 선택하여 주세요");  return;}
    if(confirm("선택한 행 ("+employeeNo.length+")건을 탈퇴처리 하시겠습니까?")){
        $("#checkList").val(employeeNo);

        var FormData = $("#requestForm").serialize();

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            data : FormData,
            url: "/basic/user_leave",
            dataType: "text",
            async : false,
            success: function (result) {
                if(result=="success"){
                    showModal("alertModal" ,"탈퇴 처리 되었습니다.");
                    setTimeout(function (){ location.href= "/basic/auth";  }, 3000);
                }else{
                    showModal("alertModal" ,"탈퇴 처리에 실패 하였습니다.");
                }
            },error: function () {
                showModal("alertModal" ,"페이지에 에러가 있습니다!");
            }
        });
    }
    setTimeout(function (){submitFlag = false;}, 3000);
}

$(document).on("keyup", ".phoneNumber", function() {
 $(this).val( $(this).val().replace(/[^0-9]/g, "").replace(/(^02|^0505|^1[0-9]{3}|^0[0-9]{2})([0-9]+)?([0-9]{4})$/,"$1-$2-$3").replace("--", "-") );
});





let submitFlag = false;

function mailSend(){

    if(submitFlag){ return false; }

    if( $("#user_email").val() == "") {
        showModal("alertModal" ,"이메일을 입력하세요.");
        return false;
    }

    submitFlag = true;

    $.ajax({
        type: "GET",
        enctype: 'multipart/form-data',
        processData: false,
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        url: "/login/findIdByEmail?email=" + $("#user_email").val(),
        dataType: "text",
        async : false,
        success: function (result) {
            if(result == "ok"){
                showModal("alertModal" ,"이메일로 id를 전송 하였습니다.");
            }else if(result == "no_exist") {
                showModal("alertModal" ,"등록되지 않은 이메일 입니다.");
            }else{
                showModal("alertModal" ,"이메일이 전송되지 않았습니다.");
            }
            setTimeout(function (){submitFlag = false;}, 3000);
        },
        error: function () {
            showModal("alertModal" ,"페이지에 에러가 있습니다!");
            setTimeout(function (){submitFlag = false;}, 3000);
        }
    });

}
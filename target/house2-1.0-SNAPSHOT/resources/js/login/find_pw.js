
let submitFlag = false;

function keyClear(){
    $("#check_num").val("");
    $("#compare_num").val("");
}

function mailKeySend(){

    if(submitFlag){ return false; }

    if($("#user_id").val() == ""){
        showModal("alertModal" ,"id를 입력하세요.");
        return false;
    }

    if($("#user_email").val() == ""){
        showModal("alertModal", "이메일을 입력하세요.");
        return false;
    }

    submitFlag = true;

    $.ajax({
        type: "GET",
        enctype: 'multipart/form-data',
        processData: false,
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        url: "/login/findPwByEmail?user_email=" + $("#user_email").val() + "&user_id=" + $("#user_id").val(),
        dataType: "text",
        async : false,
        success: function (result) {
            if(result == "no_exist"){
                showModal("alertModal" ,"아이디와 이메일을 확인하세요.");
            }else if(result != null && result != '') {
                $("#compare_num").val(result);
                $("#check_num").focus();
                showModal("alertModal" ,"인증 메일이 전송 되었습니다.");
            }else{
                showModal("alertModal" ,"이메일이 전송되지 않습니다.");
            }
        },
        error: function () {
            showModal("alertModal" ,"페이지에 에러가 있습니다!");
        }
    });
    setTimeout(function (){submitFlag = false;}, 3000);
}


function pwFind(){

    if(submitFlag){ return false; }

    if($("#user_id").val() == ""){
        showModal("alertModal" ,"id를 입력하세요.");
        return false;
    }

    if($("#user_email").val() == ""){
        showModal("alertModal", "이메일을 입력하세요.");
        return false;
    }

    if( $("#check_num").val() == "" ){
        setMessage('인증번호를 입력하세요.', document.getElementById("check_num") );
        return false;
    }

    if( $("#compare_num").val() == "" ){
        setMessage('인증메일이 전송되지 않았습니다.', document.getElementById("user_email") );
        return false;
    }

    if( $("#check_num").val() != $("#compare_num").val() ){
        setMessage('올바른 인증번호를 입력하세요.', document.getElementById("check_num") );
        return false;
    }

    submitFlag = true;
    document.getElementById('frm').submit();
    setTimeout(function (){submitFlag = false;}, 3000);
}
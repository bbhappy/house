$(document).ready(function() {


});


var giMenuDuration = 200;


// 전체 메뉴를 오른쪽으로 슬라이드하여서 보여준다.
function ShowMenu(){
	$('.menu_bg' ).css( { 'display' : 'block' } );
	$('.menu' ).css( { 'left' : '-100%' } );
	$('.menu' ).animate( { left: '0px' }, { duration: giMenuDuration } );
}

// 전체 메뉴를 왼쪽으로 슬라이드하여서 닫는다.
function HideMenu(){
	$('.menu' ).animate( { left: '-100%' }, { duration: giMenuDuration, complete:function(){ $('.menu_bg' ).css( { 'display' : 'none' } ); } } );
}

// 확장 메뉴를 보여주거나 닫는다.
function ShowSubMenu(strId){
	var lySubMenu = $(strId);

	if(lySubMenu.first().is(":hidden")){
		$(strId).slideDown( 100 );
	}
	else{
		$(strId).slideUp( 100 );
	}
}

/**
 * DESC : 공통  Ajax처리
 * 사용방법
 * 1) ajaxPostCom('/ajax/add_code', 'json', $(document.code_form).serialize(), 'add_code', fn_ajax_result);
 * 2) var fn_ajax_result = function ajaxResult(call_ajax, ajax_type, data){
 */
function ajaxPostCom(url, dataType, paramdata, fn_type, fn_ajax_result){

	var contentType    = "application/x-www-form-urlencoded; charset=UTF-8";

	$('.wrap-loading').removeClass('display-none');
	setTimeout(function(){
		$.ajax({
			async         	: true,
			type         	: "POST",
			url         	: url,
			cache         	: false,
			dataType    	: dataType,
			data         	: paramdata,
			contentType 	: contentType,
			success     	: function(data) {
				$('.wrap-loading').addClass('display-none');
				fn_ajax_result(fn_type, 'AJAX_SUCCESS', data);
			},
			complete     : function(){
				// $('.wrap-loading').addClass('display-none');
				// fn_ajax_result(fn_type, 'AJAX_COMPLETE', '');
			},
			error        : function(request,status,error){
				$('.wrap-loading').addClass('display-none');
				alert('처리 중 오류가 발생하였습니다! \n 잠시 후 다시 이용해 주세요!');
				fn_ajax_result(fn_type, 'AJAX_ERROR', "code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
			}
		});//End of Ajax
	}, 300);

}

/**
 *
 * @param command
 * @param parameter
 * @param fn_type
 * @param fn_ajax_result
 */
function ajaxRest(command, parameter, fn_type, fn_ajax_result){

	const url = "/Ajax/AjaxApi/reqRest";

	$.ajax({
		async         	: true,
		type         	: "POST",
		url        	 	: url,
		cache         	: false,
		dataType    	: "json",
		data         	: "command=" + command + "&"+ parameter,
		contentType 	: "application/x-www-form-urlencoded; charset=UTF-8",
		beforeSend: function () {
			$('.wrap-loading').removeClass('display-none');
		},
		success     	: function(data) {
			$('.wrap-loading').addClass('display-none');
			fn_ajax_result(fn_type, 'AJAX_SUCCESS', data);
		},
		complete     : function(){
			$('.wrap-loading').addClass('display-none');
		},
		error        : function(request,status,error){
			$('.wrap-loading').addClass('display-none');
			alert('처리중 오류가 발생하였습니다! \n 잠시 후 다시 이용해 주세요!');
			fn_ajax_result(fn_type, 'AJAX_ERROR', "code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
		}
	});
}

/**
 * Ajax Excel DownLoad
 * @param url
 * @param paramdata
 */
function ajaxExcel(url, paramdata, fileName){
	$('.wrap-loading').removeClass('display-none');
	setTimeout(function(){
		$.ajax({
			async         	: true,
			type         	: "POST",
			url         	: url,
			cache         	: false,
			dataType    	: "json",
			data         	: paramdata,
			success     	: function(data) {
				$('.wrap-loading').addClass('display-none');

				if(data.result == "SUCCESS"){
					var $a = $("<a>");
					$a.attr("href",data.file);
					$("body").append($a);
					$a.attr("download", fileName+".xls");
					$a[0].click();
					$a.remove();
				}else{
					alert(data.msg);
				}

			},
			error        : function(request,status,error){
				$('.wrap-loading').addClass('display-none');
				alert('처리 중 오류가 발생하였습니다! \n 잠시 후 다시 이용해 주세요!');
			}
		});//End of Ajax
	}, 300);

}

/**
 * cal = $('input[name="pp_price"]').val() * (1 - $(this).val() / 100);
 * @param price
 */
function percentMinus(totPrice, rate){
	if(rate == "" && rate == 0){
		return totPrice;
	}
	return totPrice * (1-rate/100);
}

/**
 * 1~100 사이인지 판단
 * @param num
 * @returns {boolean}
 */
function isRateRange(num){
	if(0 <= num && num <= 100){
		return true;
	}else{
		return false;
	}
}

// 앞뒤 공백제거
if (typeof String.prototype.trim !== 'function') {
	String.prototype.trim = function() {
		return this.replace(/^\s+|\s+$/g, '');
	};
}

// 이메일 형식 체크
if (typeof String.prototype.isEmail !== 'function') {
	String.prototype.isEmail = function() {
		return /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i.test(this);
	};
}

// 휴대폰번호 형식 체크
if (typeof String.prototype.isMobile !== 'function') {
	String.prototype.isMobile = function() {
		// 010은 중간이 4자리
		// 011 016 017 018 019는 중간이 3~4자리
		return /^(?:(010-?\d{4})|(01[1|6|7|8|9]-?\d{3,4}))-?\d{4}$/.test(this);

	};
}

// 전화번호 형식 체크
if (typeof String.prototype.isTel !== 'function') {
	String.prototype.isTel = function() {
		// 서울 02
		// 경기 031 / 인천 032 / 강원 033
		// 충남 041 / 대전 042 / 충북 043 / 세종 044
		// 부산 051 / 울산 052 / 대구 053 / 경북 054 / 경남 055
		// 전남 061 / 광주 062 / 전북 063 / 제주 064
		return /^0(2|3[1-3]|4[1-4]|5[1-5]|6[1-4])-?\d{3,4}-?\d{4}$/.test(this);
	};
}

// ID 형식 체크
if (typeof String.prototype.isId !== 'function') {
	String.prototype.isId = function() {
		return /^[a-z0-9_]{4,20}$/.test(this);
	};
}

// 날짜 형식 체크
if (typeof String.prototype.isDate !== 'function') {
	String.prototype.isDate = function() {
		return /^(19|20)\d{2}-?(0[1-9]|1[012])-?(0[1-9]|[12][0-9]|3[0-1])$/.test(this);
	};
}

// 숫자 3자리 단위로 , 추가
if (typeof String.prototype.getComma !== 'function') {
	String.prototype.getComma = function(n, s) {
		var num = this.trim();
		var m = parseInt((n == undefined) ? 3 : n);
		var n = (s == undefined) ? "," : s;
		var str = eval("/(-?[0-9]+)([0-9]{" + m + "})/");

		while ((str).test(num)) {
			num = num.replace((str), "$1" + n + "$2");
		}
		return num;
	};
}

// 숫자 자리수에 맞춰 반환
if (typeof String.prototype.zerofill !== 'function') {
	String.prototype.zerofill = function(cnt) {
		var digit = "";
		if (this.length < cnt) {
			for (var i = 0; i < cnt - this.length; i++) {
				digit += "0";
			}
		}
		return digit + this;
	};
}

// 확장자
if (typeof String.prototype.getExt !== 'function') {
	String.prototype.getExt = function() {
		return (this.indexOf(".") < 0) ? "" : this.substring(this.lastIndexOf(".") + 1, this.length);
	};
}

// 문자열 byte
if (typeof String.prototype.byteLength !== 'function') {
	String.prototype.byteLength = function() {
		// 한글,한자 등 = 2byte
		// 그외 = 1byte
		var cnt = 0;
		for (var i = 0; i < this.length; i++) {
			if (this.charCodeAt(i) > 127) {
				cnt += 2;
			} else {
				cnt++;
			}
		}
		return cnt;
	};
}

// 최소, 최대 바이트 길이
// '문자열'.isByteLength(min [,max])
if (String.prototype.isByteLength !== 'function') {
	String.prototype.isByteLength = function() {
		var min = arguments[0];
		var max = arguments[1] ? arguments[1] : null;
		var success = true;
		if (this.byteLength() < min) {
			success = false;
		}
		if (max && this.byteLength() > max) {
			success = false;
		}
		return success;
	};
}

// 최소, 최대 길이
// '문자열'.isLength(min [,max])
if (typeof String.prototype.isLength !== 'function') {
	String.prototype.isLength = function() {
		var min = arguments[0];
		var max = arguments[1] ? arguments[1] : null;
		var success = true;
		if (this.length < min) {
			success = false;
		}
		if (max && this.length > max) {
			success = false;
		}
		return success;
	};
}

// 공백, null 홖인
if (String.prototype.isBlank !== 'function') {
	String.prototype.isBlank = function() {
		if (this == null) {
			return true;
		}
		var str = this.trim();
		if (str == '') {
			return true;
		}
		for (var i = 0; i < str.length; i++) {
			if ((str.charAt(i) != "\t") && (str.charAt(i) != "\n") && (str.charAt(i) != "\r")) {
				return false;
			}
		}
		return true;
	};
}

// 한글
if (String.prototype.isKor !== 'function') {
	String.prototype.isKor = function() {
		return (/^[가-힣]+$/).test(this) ? true : false;
	};
}

// 영문
if (String.prototype.isEng !== 'function') {
	String.prototype.isEng = function() {
		return /^[a-zA-Z]+$/.test(this);
	};
}

// 숫자
if (String.prototype.isNum !== 'function') {
	String.prototype.isNum = function() {
		return (/^[0-9]+$/).test(this);
	};
}

// 숫자+소수점두자리
if (String.prototype.isDecimal !== 'function') {
	String.prototype.isDecimal = function() {
		return (/^\d+\.{0,1}\d{0,2}$/).test(this);
	};
}

// 한글 + 영어
if (String.prototype.isKorEng !== 'function') {
	String.prototype.isKorEng = function() {
		return (/^[가-힣a-zA-z]+$/).test(this);
	};
}

// 영문 + 숫자
if (String.prototype.isEngNum !== 'function') {
	String.prototype.isEngNum = function() {
		return /^[a-zA-Z0-9]+$/.test(this);
	};
}

// 한글 + 영문 + 숫자
if (String.prototype.isKorEng !== 'function') {
	String.prototype.isKorEng = function() {
		return (/^[가-힣a-zA-z0-9]+$/).test(this);
	};
}

/**
 * 콤마찍기
 * @param str
 * @returns {string}
 */
function comma(str) {
	str = String(str);
	return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
}


/**
 * 콤마풀기
 * @param str
 * @returns {string}
 */
function uncomma(str) {
	str = String(str);
	return str.replace(/[^\d]+/g, '');
}

/**
 * 빈값 체크
 * @param object
 * @param msg
 * @returns {boolean}
 */
function emptyCheck(object, msg){

	if(object.val() == ""){
		alert(msg);
		object.focus();
		return true;
	}
	return false;
}


modelMsgReload=function(code,message){
	$('#alertModal-title').html("<p class='p1'>"+code+"</p>");
	$('#alertModal-body').html("<p class='p1'>"+message+"</p>");
	$('#alertModal').modal('show');
	$(function(){$('#alertModal').on('hidden.bs.modal',function(){})});

};

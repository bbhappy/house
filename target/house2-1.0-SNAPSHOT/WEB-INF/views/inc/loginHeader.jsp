<!DOCTYPE html>
<html lang="ko">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, shrink-to-fit=no">
  <meta http-equiv="imagetoolbar" content="no" />
  <meta http-equiv="Cache-Control" content="No-Cache" />
  <meta HTTP-EQUIV="Pragma" CONTENT="no-cache"/>
  <meta HTTP-EQUIV="Expires" CONTENT="-1"/>

  <%@ page contentType="text/html;charset=utf-8" %>
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

  <!-- 위 3개의 메타 태그는 *반드시* head 태그의 처음에 와야합니다; 어떤 다른 콘텐츠들은 반드시 이 태그들 *다음에* 와야 합니다 -->
  <title>다정다감</title>
  <meta name="description" content=""/>
  <meta name="keywords" content=""/>

  <!--font-->
  <link rel="stylesheet" type="text/css" href="https://cdn.rawgit.com/moonspam/NanumSquare/master/nanumsquare.css">
  <link href="https://fonts.googleapis.com/css?family=Noto+Sans+KR:100,300,400,500,700,900&display=swap" rel="stylesheet">

  <script src="<c:url value="/resources/js/common/jquery-3.4.1.min.js" />"></script>

  <!-- CSS , JS -->
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.min.css">
  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="<c:url value="/resources/css/admin/bootstrap.custom.css?v2r" />">
  <link rel="stylesheet" href="<c:url value="/resources/css/admin/bootstrap.ie.css?v2" />">

  <!-- 기본 CSS -->
  <link rel="stylesheet" href="<c:url value="/resources/css/admin/common.css" />">

  <!-- View -->
  <script src="<c:url value="/resources/js/common/common.js" />"></script>
  <script src="<c:url value="/resources/js/common/common2.js" />"></script>

  <!-- Number js -->
  <script type="text/javascript" src="<c:url value="/resources/js/common/jquery.number.js" />"></script>

  <!-- Form Validation -->
  <script type="text/javascript" src="<c:url value="/resources/js/common/jquery.validate.js" />"></script>
  <script type="text/javascript" src="<c:url value="/resources/js/common/jquery.validate.add-method.js" />"></script>

  <!-- Toast Ui -->
  <link rel="stylesheet" href="https://uicdn.toast.com/chart/latest/toastui-chart.min.css" />
  <script src="https://uicdn.toast.com/chart/latest/toastui-chart.min.js"></script>
  <script src="https://unpkg.com/babel-standalone@6.26.0/babel.min.js"></script>

  <!-- Select2 -->
  <script type="text/javascript" src="<c:url value="/resources/js/common/select2.min.js" />"></script>
  <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/admin/select2.min.css"/>">
<%@ page import="java.time.LocalDateTime" %>
<%@ page contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<%@ include file="../inc/adminHeader.jsp" %>
<script type="text/javascript" src="<c:url value="/resources/js/board/list.js" />" ></script>

<section>
    <div class="col-xl-10 col-lg-9 col-md-8 ml-auto bg-light fixed-top py-2 top-navbar">
        <div class="row align-items-center">
            <div class="col-md-4">
                <h4 class="text-secondary text-uppercase mb-2 mt-2">${u_menu_navi}</h4>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-10 col-md-8 col-lg-9 ml-auto">
                <div class="row pt-md-5 mt-md-3 mb-5">
                    <div class="col-xl-12 col-md-12 col-sm-12 p2 mt-5">

                        <form name="requestForm" id="requestForm" method="post">
                            <div class="row justify-content-between col-xl-12 col-md-12 col-sm-12 mb-2">
                                <div class="px-3">
                                    <span id="searchDetailLabel" data-toggle="dropdown" class="btn btn-sm btn-outline-primary dropdown-toggle"><i class="fas fa-search"></i></span>
                                    <div class="dropdown-menu px-2 container-fluid" aria-labelledby="searchDetailLabel" style="width:70%">
                                        <div class="form-row mb-2 ml-2">
                                            <div class="col-sm-12">
                                                <div class="input-group input-group-lg pt-1">
                                                    <div class="input-group-prepend"><span class="input-group-text">작 성 자</span></div>
                                                    <input type="text" name="search_nm" id="search_nm" value="${search_nm}" onkeyup="javascript:if(event.keyCode == 13){searchDetail();}" class="form-control" aria-label="이름" aria-describedby="이름">
                                                </div>
                                                <div class="input-group input-group-lg pt-1">
                                                    <div class="input-group-prepend"><span class="input-group-text">제목</span></div>
                                                    <input type="text" name="search_title" id="search_title" value="${search_content}" onkeyup="javascript:if(event.keyCode == 13){searchDetail();}" class="form-control" aria-label="아이디" aria-describedby="아이디">
                                                </div>
                                            </div>
                                        </div>
                                        <a type="button" class="btn btn-sm btn-outline-primary btn-block active" onclick="searchDetail();" aria-label="검색"> 검색 </a>
                                    </div>
                                </div>
                                <c:if test="${u_auth_new eq 'Y'}" >
                                    <button type="button" class="btn btn-sm btn-outline-info" onclick="boardWrite()">글쓰기</button>
                                </c:if>
                            </div>
                            <table class="table table-sm  table-bordered table-hover" id="boardTable">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col" class="text-center">작성자</th>
                                        <th scope="col" class="text-center">제목</th>
                                        <th scope="col" class="text-center">등록일</th>
                                        <th scope="col" class="text-center d-none d-lg-table-cell">조회</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:if test="${totalCount gt 0 }">
                                    <c:forEach items="${listBoard}" var="board" >

                                        <tr class="text-align bg-white" style="cursor:pointer">
                                            <td class="<c:if test="${u_auth_mod eq 'Y'}" >rowTr</c:if>" >
                                                <input type="hidden" id="chk_board_no" value="${board.getIdx()}"/>
                                                ${board.getWriter()}
                                            </td>
                                            <td class="<c:if test="${u_auth_mod eq 'Y'}" >rowTr</c:if>" >
                                                    ${board.getTitle()}
                                            </td>
                                            <td class="<c:if test="${u_auth_mod eq 'Y'}" >rowTr</c:if>" >
                                                <fmt:parseDate value="${board.getReq_dt()}" pattern="yyyy-MM-dd'T'HH:mm:ss" var="parsedDateTime" type="both" />
                                                <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${parsedDateTime}" />
                                            </td>
                                            <td class="<c:if test="${u_auth_mod eq 'Y'}" >rowTr</c:if> d-none d-lg-table-cell" >
                                                <fmt:formatNumber value="${board.getView()}" />
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </c:if>
                                <c:if test="${totalCount eq 0 }">
                                    <tr style="text-align: center">
                                        <td scope='row' colspan='10'>검색 결과가 없습니다.</td>
                                    </tr>
                                </c:if>
                                </tbody>
                            </table>
                            <div style="margin-left:38%">
                                <ul class="pagination">
                                    <c:if test="${pageMaker.prev}">
                                        <li class="page-item"><a class="page-link" href="<c:url value="/board/listPage?pageNum=1"/>">맨앞</a></li>
                                    </c:if>
                                    <c:if test="${pageMaker.prev}">
                                        <li class="page-item">
                                            <a class="page-link" href="<c:url value="/board/listPage?pageNum=${pageMaker.cri.pageNum-1}"/>">이전</a>
                                        </li>
                                    </c:if>
                                    <c:if test="${pageMaker.endPage gt 1}">
                                        <c:forEach var="num" begin="${pageMaker.startPage}" end="${pageMaker.endPage}">
                                            <li class="page-item ${pageMaker.cri.pageNum == num? "active":"" }">
                                                <a class="page-link" href=" <c:url value="/board/listPage?pageNum=${num}"/>">${num}</a>
                                            </li>
                                        </c:forEach>
                                    </c:if>
                                    <c:if test="${pageMaker.next}">
                                        <li class="page-item"><a class="page-link" href="<c:url value="/board/listPage?pageNum=${pageMaker.cri.pageNum+1}"/>">다음</a></li>
                                    </c:if>
                                    <c:if test="${pageMaker.next}">
                                        <li class="page-item"><a class="page-link" href="<c:url value="/board/listPage?pageNum=${pageMaker.lastPage}"/>">맨끝</a></li>
                                    </c:if>
                                </ul>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<%@ include file="../inc/adminFooter.jsp" %>

</body>
</html>


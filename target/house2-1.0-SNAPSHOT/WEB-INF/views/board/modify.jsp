<%@ page contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>

<%@ include file="../inc/adminHeader.jsp" %>
<script type="text/javascript" src="<c:url value="/resources/js/board/modify.js" />" ></script>

<!-- 서머노트  -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script src=" https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.18/lang/summernote-ko-KR.min.js"></script>

<%
    String referer = request.getHeader("REFERER");
    System.out.println(referer);
    if(referer == null){
%>
<script>
    alert("URL을 직접 입력해서 접근하셨습니다.\n정상적인 경로를 통해 다시 접근해 주세요.");
    document.location.href="/";
</script>
<%
        return;
    }
%>

<section>
    <div class="col-xl-10 col-lg-9 col-md-8 ml-auto bg-light fixed-top py-2 top-navbar">
        <div class="row align-items-center">
            <div class="col-md-4">
                <h4 class="text-secondary text-uppercase mb-2 mt-2">게시판->자유게시판(글쓰기)</h4>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-10 col-md-8 col-lg-9 ml-auto">
                <div class="row pt-md-5 mt-md-3 mb-5">
                    <div class="col-xl-12 col-md-12 col-sm-12">

                        <form id="articleForm">
                            <input type="hidden" name="idx" value="${boardList.idx}" />

                            <h3 style="margin-bottom: 25px;"></h3>
                            <div class="form-group">
                                <input type="text" class="form-control" name="title" id="title" value="${boardList.title}" placeholder="제목" maxlength="30" required>
                            </div>
                            <div class="form-group">
                                <div id="test_cnt">( 0 / 1000 )</div>
                                <textarea class="form-control" name="content" id="summernote">${boardList.content}</textarea>
                            </div>
                        </form>
                        <c:if test="${boardList.writer_id eq sessionScope.user_id }" >
                            <button class="btn btn-info btn-md" onclick="boardModify()">수정</button>
                        </c:if>
                        <button class="btn btn-danger btn-md" onclick="location.href='/board/listPage'">목록으로</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $("#summernote").summernote({
        // 에디터 높이
        height: 600,
        placeholder:"1000자 까지 가능",
        // 에디터 한글 설정
        lang: "ko-KR",
        // 에디터에 커서 이동 (input창의 autofocus라고 생각하시면 됩니다.)
        focus : true,
        toolbar: [
            // 글꼴 설정
            ['fontname', ['fontname']],
            // 글자 크기 설정
            ['fontsize', ['fontsize']],
            // 굵기, 기울임꼴, 밑줄,취소 선, 서식지우기
            ['style', ['bold', 'italic', 'underline','strikethrough', 'clear']],
            // 글자색
            ['color', ['forecolor','color']],
            // 표만들기
            ['table', ['table']],
            // 글머리 기호, 번호매기기, 문단정렬
            ['para', ['ul', 'ol', 'paragraph']],
            // 줄간격
            ['height', ['height']],
            // 그림첨부, 링크만들기, 동영상첨부
            ['insert',['picture','link','video']]
            // 코드보기, 확대해서보기, 도움말
            //['view', ['codeview','fullscreen', 'help']]
        ],
        callbacks : {
            onImageUpload : function(files, editor, welEditable) { // 파일 업로드(다중업로드를 위해 반복문 사용)
                for (var i = files.length - 1; i >= 0; i--) {
                    uploadSummernoteImageFile(files[i], this);
                }
            },
            onChange:function(contents, $editable){ //텍스트 글자수 및 이미지등록개수
                setContentsLength(contents, 0);
            }
        },
        fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New','맑은 고딕','궁서','굴림체','굴림','돋음체','바탕체'], // 추가한 글꼴
        // 추가한 폰트사이즈
        fontSizes: ['8','9','10','11','12','14','16','18','20','22','24','28','30','36','50','72']
    });
</script>


<%@ include file="../inc/adminFooter.jsp" %>

</body>
</html>


<%@ page contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<%@ include file="../inc/adminHeader.jsp" %>
<script type="text/javascript" src="<c:url value="/resources/js/basic/auth_modify.js" />" ></script>

<%
    String referer = request.getHeader("REFERER");
    System.out.println(referer);
    if(referer == null){
%>
<script>
    alert("URL을 직접 입력해서 접근하셨습니다.\n정상적인 경로를 통해 다시 접근해 주세요.");
    document.location.href="/";
</script>
<%
        return;
    }
%>


<section>
    <div class="col-xl-10 col-lg-9 col-md-8 ml-auto bg-light fixed-top py-2 top-navbar">
        <div class="row align-items-center">
            <div class="col-md-4">
                <h4 class="text-secondary text-uppercase mb-2 mt-2">환경설정 -> 권한 수정</h4>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-10 col-md-8 col-lg-9 ml-auto">
                <div class="row pt-md-5 mt-md-3 mb-5">
                    <div class="col-xl-12 col-md-12 col-sm-12 p2 mt-5">

                        <form name="requestForm" id="requestForm" >
                            <input type="hidden" name="it_user_id" value="${it_user_id}" />
                            <div class="col-xl-12 col-md-12 col-sm-12 mb-2">
                                <button type="button" class="btn btn-sm btn-outline-danger" onclick="location.href='/basic/auth'">뒤로가기</button>
                                <button type="button" class="btn btn-sm btn-outline-secondary" onclick="userAuthSave()">${it_user_nm} 권한 수정</button>
                            </div>
                            <table class="table table-sm  table-bordered table-hover" id="authTable">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col" class="text-center">메뉴</th>
                                        <th scope="col" class="text-center">조회</th>
                                        <th scope="col" class="text-center">등록</th>
                                        <th scope="col" class="text-center">수정</th>
                                        <th scope="col" class="text-center">삭제</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${allMenuList}" var="allMenuList" varStatus="status">
                                        <tr>
                                            <c:if test="${allMenuList.mm_level eq 1}">
                                                <td>${allMenuList.mm_name}</td>
                                                <td colspan="4">
                                                    <input type="checkbox" data-toggle="toggle" data-onstyle="outline-success" data-offstyle="outline-danger" name="s_mu_view" value="${allMenuList.mm_code}"
                                                        <c:forEach items="${userAuthList}" var="userAuthList">
                                                            <c:if test="${allMenuList.mm_parent eq userAuthList.mm_parent}">
                                                                <c:if test="${allMenuList.mm_code eq userAuthList.mm_code}">
                                                                    <c:if test="${userAuthList.mu_view eq 'Y'}"> checked </c:if>
                                                                </c:if>
                                                            </c:if>
                                                        </c:forEach>
                                                    />
                                                </td>
                                            </c:if>
                                            <c:if test="${allMenuList.mm_level ne 1}">
                                                <td>&nbsp;<i class="fa fa-minus"></i>&nbsp;&nbsp;${allMenuList.mm_name}</td>
                                                <td>
                                                    <input type="checkbox" data-toggle="toggle" data-onstyle="outline-success" data-offstyle="outline-danger" name="s_mu_view" value="${allMenuList.mm_code}"
                                                        <c:forEach items="${userAuthList}" var="userAuthList">
                                                            <c:if test="${allMenuList.mm_parent eq userAuthList.mm_parent}">
                                                                <c:if test="${allMenuList.mm_code eq userAuthList.mm_code}">
                                                                    <c:if test="${userAuthList.mu_view eq 'Y'}"> checked </c:if>
                                                                </c:if>
                                                            </c:if>
                                                        </c:forEach>
                                                     />
                                                </td>
                                                <td>
                                                    <input type="checkbox" data-toggle="toggle" data-onstyle="outline-success" data-offstyle="outline-danger" name="s_mu_new" value="${allMenuList.mm_code}"
                                                        <c:forEach items="${userAuthList}" var="userAuthList">
                                                            <c:if test="${allMenuList.mm_parent eq userAuthList.mm_parent}">
                                                                <c:if test="${allMenuList.mm_code eq userAuthList.mm_code}">
                                                                    <c:if test="${userAuthList.mu_new eq 'Y'}"> checked </c:if>
                                                                </c:if>
                                                            </c:if>
                                                        </c:forEach>
                                                    />
                                                </td>
                                                <td>
                                                    <input type="checkbox" data-toggle="toggle" data-onstyle="outline-success" data-offstyle="outline-danger" name="s_mu_mod" value="${allMenuList.mm_code}"
                                                        <c:forEach items="${userAuthList}" var="userAuthList">
                                                            <c:if test="${allMenuList.mm_parent eq userAuthList.mm_parent}">
                                                                <c:if test="${allMenuList.mm_code eq userAuthList.mm_code}">
                                                                    <c:if test="${userAuthList.mu_mod eq 'Y'}"> checked </c:if>
                                                                </c:if>
                                                            </c:if>
                                                        </c:forEach>
                                                    />
                                                </td>
                                                <td>
                                                    <input type="checkbox" data-toggle="toggle" data-onstyle="outline-success" data-offstyle="outline-danger" name="s_mu_del" value="${allMenuList.mm_code}"
                                                        <c:forEach items="${userAuthList}" var="userAuthList">
                                                            <c:if test="${allMenuList.mm_parent eq userAuthList.mm_parent}">
                                                                <c:if test="${allMenuList.mm_code eq userAuthList.mm_code}">
                                                                    <c:if test="${userAuthList.mu_del eq 'Y'}"> checked </c:if>
                                                                </c:if>
                                                            </c:if>
                                                        </c:forEach>
                                                    />
                                                </td>
                                            </c:if>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<%@ include file="../inc/adminFooter.jsp" %>

</body>
</html>


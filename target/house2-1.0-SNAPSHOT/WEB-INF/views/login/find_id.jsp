<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="../inc/loginHeader.jsp" %>
<link href="<c:url value="/resources/css/login/signup.css" />" rel="stylesheet">
<script type="text/javascript" src="<c:url value="/resources/js/login/find_id.js" />" ></script>

</head>
<body>
<div class="wrapper">
    <div class="login" >
        <div class="title">아이디 찾기</div>
        <div id="msg" class="msg">
            <i class="fa fa-exclamation-circle"> 등록된 이메일로 id를 알려 드립니다.</i>
        </div>
        <input class="input-info form-control" type="email" name="user_email" id="user_email" placeholder="등록된 이메일" maxlength="35">

        <div class="btn-group col-xl-12 col-md-12 col-sm-12" role="group">
            <button class="btn btn-default btn-lg btn-info mr-1"  onclick="mailSend()">메일전송</button>
            <button class="btn btn-default btn-lg btn-danger" onclick="history.back(-1)">로그인페이지</button>
        </div>
    </div>
</div>

<%@ include file="../inc/loginFooter.jsp" %>
</body>
</html>

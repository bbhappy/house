<%@ page contentType="text/html;charset=utf-8" %>

<%@ include file="../inc/loginHeader.jsp" %>
<link href="<c:url value="/resources/css/login/login.css" />" rel="stylesheet">
<script type="text/javascript" src="<c:url value="/resources/js/login/login.js" />"></script>

</head>
<body>

<div class="wrapper">
    <form class="login" id="loginForm" class="form-signin">
      <span class="title">
          <div class="logo">
              <img src="/resources/images/login_mark.png">
          </div>
      </span>

      <input type="text" id="user_id" name="user_id" class="form-control input_ty1" onkeyup="javascript:if(event.keyCode == 13){ formCheck(); }" placeholder="사용자 ID" maxlength="15" required>
      <div id="inputNoDiv" class="msg_type1 alert-dismissible fade d-none p0" role="alert">
        사용자ID 를 입력하세요.
      </div>
      <i class="fa fa-user"></i>
      <input type="password" id="user_pw" name="user_pw" class="form-control input_ty1" onkeyup="javascript:if(event.keyCode == 13){ formCheck(); }" placeholder="비밀번호" maxlength="10" required>
      <div id="inputPassDiv" class="msg_type1 alert-dismissible fade d-none p0" role="alert">
        비밀번호 를 입력하세요.
      </div>

      <i class="fa fa-key"></i>
      <button type="button" onclick="formCheck()">
        <i class="spinner"></i>
        <span class="state">로그인</span>
      </button>

      <div>
        <a href="/login/findId">아이디 찾기</a> |
        <a href="/login/findPw">비밀번호 찾기</a> |
        <a href="/login/signup">사용자 등록</a>
      </div>

      <div id="resultMessageDiv" class="msg_type1 alert-dismissible fade d-none" role="alert">
        <strong id="resultMessage">- 로그인실패 !</strong>
      </div>
    </form>
</div>

<%@ include file="../inc/loginFooter.jsp" %>
</body>
</html>
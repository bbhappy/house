<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="../inc/loginHeader.jsp" %>
<link href="<c:url value="/resources/css/login/signup.css" />" rel="stylesheet">
<script type="text/javascript" src="<c:url value="/resources/js/login/change_pw.js" />" ></script>

</head>
<body>

<div class="wrapper">
    <div class="login" >
        <div class="title">비밀번호 변경</div>
        <div id="msg" class="msg">
            <i class="fa fa-exclamation-circle"> 비밀번호를 변경합니다.</i>
        </div>
        <form id="frm" action="/login/changePw" method="post">
            <input class="input-info form-control" type="text" name="user_id" id="user_id" value="<%=request.getParameter("user_id") %>" placeholder="사용자ID" maxlength="15" readonly/>
            <input class="input-info form-control" type="text" name="user_email" id="user_email" value="<%=request.getParameter("user_email") %>" placeholder="등록된 이메일" maxlength="30" readonly/>
            <input class="input-info form-control" type="password" name="user_pw" id="user_pw" placeholder="변경할 비밀번호 5~8자리 영어, 숫자, (!@$%^&*)" maxlength="10">
            <input class="input-info form-control" type="password" name="check_pw" id="check_pw" placeholder="비밀번호 확인 5~8자리 영어, 숫자, (!@$%^&*)" maxlength="10">
        </form>

        <div class="btn-group col-xl-12 col-md-12 col-sm-12" role="group">
            <button class="btn btn-default btn-lg btn-info mr-1" onclick="pwChange()">비밀변호 변경</button>
            <button class="btn btn-default btn-lg btn-info" onclick="location.href='/';">로그인페이지</button>
        </div>
    </div>
</div>

<%@ include file="../inc/loginFooter.jsp" %>
</body>
</html>
